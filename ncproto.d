/* Written by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
// very simple serializer
module ncproto is aliced;

import iv.vfs.io;
import iv.ncrpc;

//import sockchan;


// ////////////////////////////////////////////////////////////////////////// //
struct InjCodeInfo {
  bool hasWine;
  bool hasMono;
}


void ncSendPingback() (UDSocket sk, in auto ref InjCodeInfo nfo) {
  sk.writeNum!ubyte(0); // version
  sk.ncser(nfo);
}


void ncReceivePingback (UDSocket sk, ref InjCodeInfo nfo) {
  if (sk.readNum!ubyte != 0) throw new Exception("invalid protocol version");
  sk.ncunser(nfo);
}


// ////////////////////////////////////////////////////////////////////////// //
public struct AssemblyInfo {
  uint id;
  string name;
}


// ////////////////////////////////////////////////////////////////////////// //
public struct NCClassInfo {
  uint id;
  uint asmid;
  string name;
  string nspace;
  // constructed
  @NCIgnore string asmname;
  @NCIgnore string fullname;
  @NCIgnore string fullnamenoasm;

  bool isNameEqu (const(char)[] name) const {
    import std.algorithm : endsWith;
    if (!fullname.endsWith(name)) return false;
    if (fullname.length == name.length) return true;
    if (fullname[$-name.length-1] != '.') return false;
    return true;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public struct FieldInfo {
  uint type;
  uint ofs;
  uint flags;
  string name;
  string typename;
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared string delegate (uint asmid) getAssemblyName;
__gshared AssemblyInfo[] delegate () enumAssemblies;
__gshared NCClassInfo[] delegate (uint asmid=0) getClassList;
__gshared FieldInfo[] delegate (uint clsid) getClassFields;
