/* Written by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module addrstore is aliced;


// ////////////////////////////////////////////////////////////////////////// //
enum VType : ubyte {
  Byte   = 1<<0, // 8 bits
  Word   = 1<<1, // 16 bits
  Int    = 1<<2, // 32 bits
  Long   = 1<<3, // 64 bits
  Float  = 1<<4, // 32 bits
  Double = 1<<5, // 64 bits
  IMask = 0b00001111,
  FMask = 0b00110000,
}


uint maxVTSize (ubyte vt) {
  if (vt&VType.Double) return 8;
  if (vt&VType.Long) return 8;
  if (vt&VType.Float) return 4;
  if (vt&VType.Int) return 4;
  if (vt&VType.Word) return 2;
  return 1;
}


string vtName (ubyte vt) {
  if (vt&VType.Double) return "f8";
  if (vt&VType.Long) return "i8";
  if (vt&VType.Float) return "f4";
  if (vt&VType.Int) return "i4";
  if (vt&VType.Word) return "i2";
  if (vt&VType.Byte) return "i1";
  return "b0";
}


// ////////////////////////////////////////////////////////////////////////// //
// pool data format:
// first 4 bytes: base address
// next byte; value type
// data loop:
//  next byte:
//    ==0: 3-byte offset to next address follows
//    ==255: 4-byte next address follows
//    other: this is byte offset to next address
//  next byte: value type


// ////////////////////////////////////////////////////////////////////////// //
static struct AddrVal { uint addr; ubyte vt; }

alias ASRange = ASRangeImpl!false;
alias ASRangeD = ASRangeImpl!true;

struct ASRangeImpl(bool destructive) {
private:
  private import core.stdc.stdlib : free;
  @disable this (this);
  AddrStore.PoolHead* ph;
  ubyte* dp; // data pointer in ph
  uint dataLeft; // in ph
  uint addr; // front element -- address
  ubyte vt; // front element -- vtype
  uint len; // addresses left (total)

private:
  void finish () nothrow @nogc {
    static if (destructive) {
      // free all remaining pools
      import core.stdc.stdlib : free;
      while (ph !is null) {
        auto np = ph.next;
        free(ph);
        ph = np;
      }
    } else {
      ph = null;
    }
    // just in case
    dp = null;
    dataLeft = 0;
    addr = 0;
    vt = 0;
    len = 0;
  }

public:
  this (ref AddrStore fv) nothrow @nogc { setup(fv); }
  ~this () { finish(); }

  void setup (ref AddrStore fv) nothrow @nogc {
    finish();
    ph = fv.firstPool;
    len = fv.addrCount;
    static if (destructive) fv.reset(); // reset store, so it can accept new addresses
    if (len) {
      ++len; // 'cause popFront will dec it
      popFront();
    }
  }

  @property bool empty () const pure { pragma(inline, true); return (len == 0); }
  @property AddrVal front () const pure { pragma(inline, true); return AddrVal(addr, vt); }
  @property uint length () const pure { pragma(inline, true); return len; }

  void popFront () {
    if (len <= 1) { finish(); return; } // no more
    --len; // front item removed
    while (ph !is null) {
      if (dp is null || dataLeft < 2) {
        if (dp !is null) {
          // free current block, get next one
          static if (destructive) {
            import core.stdc.stdlib : free;
            auto np = ph.next;
            free(ph);
            ph = np;
          } else {
            ph = ph.next;
          }
          dp = null;
          if (ph is null) break;
        }
        // first entry of new block
        dataLeft = ph.used;
        if (dataLeft < 5) { dataLeft = 0; continue; } // out of data
        dp = ph.data.ptr;
        addr = *cast(uint*)dp; dp += 4;
        vt = *dp++;
        dataLeft -= 5;
        return;
      }
      uint ofs = *dp++;
      if (ofs != 0 && ofs != 255) {
        // short
        addr += ofs;
        vt = *dp++;
        dataLeft -= 2;
      } else if (ofs == 255) {
        // new address
        if (dataLeft < 6) { dataLeft = 0; continue; } // out of data in this chunk
        addr = *dp++;
        addr |= (*dp++)<<8;
        addr |= (*dp++)<<16;
        addr |= (*dp++)<<24;
        vt = *dp++;
        dataLeft -= 6;
      } else if (ofs == 0) {
        // long
        if (dataLeft < 5) { dataLeft = 0; continue; } // out of data in this chunk
        ofs = *dp++;
        ofs |= (*dp++)<<8;
        ofs |= (*dp++)<<16;
        addr += ofs;
        vt = *dp++;
        dataLeft -= 5;
      } else {
        assert(0, "wtf?!");
      }
      return;
    }
    finish(); // alas
  }
}


// ////////////////////////////////////////////////////////////////////////// //
struct AddrStore {
private:
  enum PoolSize = 32*1024;

  align(1) struct PoolHead {
  align(1):
    PoolHead* next; // null for last one
    uint size; // data size, excluding head
    uint used; // data used
    // data follows
    ubyte[0] data;
  nothrow @trusted @nogc:
    void putByte (ubyte b) {
      if (used >= size || size-used < 1) assert(0, "wtf?!");
      *(data.ptr+(used++)) = b;
    }
  }

  PoolHead* firstPool;
  PoolHead* lastPool;
  uint poolsUsed;
  uint lastAddr;
  uint addrCount;

  void addPool () {
    import core.stdc.stdlib : malloc;
    PoolHead* ph = cast(PoolHead*)malloc(PoolSize);
    if (ph is null) assert(0, "out of memory");
    if (lastPool !is null) lastPool.next = ph; else firstPool = ph;
    lastPool = ph;
    ph.next = null;
    ph.size = PoolSize-PoolHead.sizeof;
    ph.used = 0;
    ++poolsUsed;
  }

  // reset pools, but don't clear
  void reset () nothrow @trusted @nogc {
    firstPool = lastPool = null;
    poolsUsed = 0;
    lastAddr = 0;
    addrCount = 0;
  }

public:
  @disable this (this);
  ~this () { clear(); }

  uint count () const pure nothrow @safe @nogc { pragma(inline, true); return addrCount; }
  uint poolCount () const pure nothrow @safe @nogc { pragma(inline, true); return poolsUsed; }

  // not opIndex, 'cause it is slow as hell
  AddrVal at (uint idx) {
    if (idx >= addrCount) throw new Exception("invalid index");
    auto rng = ASRange(this);
    while (idx > 0) {
      if (rng.empty) assert(0, "wtf?!");
      rng.popFront;
      --idx;
    }
    if (rng.empty) assert(0, "wtf?!");
    return rng.front;
  }

  void clear () {
    import core.stdc.stdlib : free;
    while (firstPool !is null) {
      auto cp = firstPool;
      firstPool = cp.next;
      free(cp);
    }
    reset();
  }

  void addAddress (uint addr, ubyte vt) {
    // we can waste up to 4 bytes with this; idc
    if (lastPool !is null && lastPool.size-lastPool.used >= 6) {
      if (lastAddr <= addr && addr-lastAddr <= 0xffffff) {
        uint diff = addr-lastAddr;
        if (diff == 0 || diff > 254) {
          // long form
          lastPool.putByte(0);
          lastPool.putByte(diff&0xff);
          lastPool.putByte((diff>>8)&0xff);
          lastPool.putByte((diff>>16)&0xff);
        } else {
          // short form
          lastPool.putByte(cast(ubyte)diff);
        }
      } else {
        // new addr form
        lastPool.putByte(255);
        lastPool.putByte(addr&0xff);
        lastPool.putByte((addr>>8)&0xff);
        lastPool.putByte((addr>>16)&0xff);
        lastPool.putByte((addr>>24)&0xff);
      }
    } else {
      // new pool
      addPool();
      *cast(uint*)(lastPool.data.ptr) = addr;
      lastPool.used = 4;
    }
    lastPool.putByte(vt);
    lastAddr = addr;
    ++addrCount;
  }
}
