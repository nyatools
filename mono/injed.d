module injed;

import core.runtime : Runtime, rt_init;
import core.thread;
import core.time;

import std.algorithm;

import iv.olly.assembler;
import iv.vfs.io;
import iv.ncrpc;

//import sockchan;
import ncproto;
import monox;

__gshared char* nyaSockName;
__gshared UDSocket nyaSock;


void gcCollect () {
  import core.memory : GC;
  GC.collect();
  GC.minimize();
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared string[] delegate () listEndpoints;

string[] listEndpointsEP () { return rpcEndpointNames; }


// ////////////////////////////////////////////////////////////////////////// //
void sendHello () {
  InjCodeInfo nfo;
  nfo.hasWine = isWineAPILoaded;
  nfo.hasMono = isMonoAPILoaded;
  nyaSock.ncSendPingback(nfo);
}


// ////////////////////////////////////////////////////////////////////////// //
void commThread () {
  import std.functional : toDelegate;
  /*
  rpcRegisterModuleEndpoints!monox("mono.");
  //rpcRegisterEndpoint!listEndpoints;
  rpcRegisterModuleEndpoints!(mixin(__MODULE__));
  */
  rpcRegisterEndpoint!getAssemblyName(toDelegate(&getAssemblyNameEP));
  rpcRegisterEndpoint!enumAssemblies(toDelegate(&enumAssembliesEP));
  rpcRegisterEndpoint!getClassList(toDelegate(&getClassListEP));
  rpcRegisterEndpoint!getClassFields(toDelegate(&getClassFieldsEP));
  rpcRegisterEndpoint!listEndpoints(toDelegate(&listEndpointsEP));
  try {
    writeln("D: communication thread started.");
    loadWineAPI();
    if (isWineAPILoaded) loadMonoAPI();
    writeln("D: API scan complete.");
    writeln("D: GC ok.");
    writeln("D: sending pingback info...");
    sendHello();
    writeln("D: pingback info sent.");
    for (;;) {
      auto cmd = nyaSock.readNum!ushort;
      if (cmd != RPCommand.Call) throw new Exception("invalid command");
      nyaSock.rpcProcessCall;
    }
  } catch (Throwable e) {
    // here, we are dead and fucked (the exact order doesn't matter)
    import core.stdc.stdio : fprintf, stderr;
    auto s = e.toString();
    fprintf(stderr, "\n=== FATAL ===\n%.*s\n", cast(uint)s.length, s.ptr);
    /*
    import core.stdc.stdlib : abort;
    import core.stdc.stdio : fprintf, stderr;
    import core.memory : GC;
    GC.disable(); // yeah
    thread_suspendAll(); // stop right here, you criminal scum!
    auto s = e.toString();
    fprintf(stderr, "\n=== FATAL ===\n%.*s\n", cast(uint)s.length, s.ptr);
    abort(); // die, you bitch!
    */
  }
  gcCollect();
}


extern(C) void* pthreadThread (void*) nothrow {
  import core.stdc.stdio;
  import std.string : fromStringz;
  try {
    thread_setGCSignals(30, 31); // hope this works
    printf("D: initializing runtime...\n");
    Runtime.initialize();
    //rt_init();
    printf("D: runtime initialized.\n");
    version(none) {
      thread_detachThis(); // druntime does this automatically, but we are pthread's property
      return null;
    }
    thread_attachThis(); // this is good thread
    writefln("D: sockname is '%s'", nyaSockName.fromStringz);
    if (nyaSockName !is null && nyaSockName[0]) {
      import core.stdc.string;
      memset(&nyaSock, 0, nyaSock.sizeof);
      nyaSock.connect(nyaSockName.fromStringz);
      commThread();
    }
  } catch (Throwable e) {
    // here, we are dead and fucked (the exact order doesn't matter)
    import core.stdc.stdlib : abort;
    import core.stdc.stdio : fprintf, stderr;
    import core.memory : GC;
    GC.disable(); // yeah
    //thread_suspendAll(); // stop right here, you criminal scum!
    //auto s = e.toString();
    //fprintf(stderr, "\n=== FATAL ===\n%.*s\n", cast(uint)s.length, s.ptr);
    fprintf(stderr, "\n=== FATAL ===\n");
    abort(); // die, you bitch!
  }
  thread_detachThis(); // druntime does this automatically, but we are pthread's property
  return null;
}


// return 0 to indicate failure
extern(C) int initme (const(char)* sockname) nothrow @nogc {
  import core.sys.posix.pthread;
  import core.stdc.stdlib : malloc;
  import core.stdc.string : strlen, strcpy;
  //thread_detachThis(); // this is alien thread, no need to do anything with it
  if (sockname !is null) {
    auto len = strlen(sockname);
    nyaSockName = cast(char*)malloc(len+1);
    strcpy(nyaSockName, sockname);
  } else {
    nyaSockName = null;
  }
  pthread_t trd;
  return (pthread_create(&trd, null, &pthreadThread, null) ? 0 : 1);
}
