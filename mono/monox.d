module monox is aliced;
private:

import std.string : fromStringz;
import iv.vfs.io;

//import iv.ncserial;
import ncproto;


// ////////////////////////////////////////////////////////////////////////// //
extern(C) nothrow @nogc {
  enum RTLD_LAZY = 0x00001; // POSIX
  extern void* __libc_dlopen_mode (const(char)* name, int mode);
  extern void* __libc_dlsym (void* soh, const(char)* name);
  extern int __libc_dlclose (void* soh);
}


// ////////////////////////////////////////////////////////////////////////// //
alias BOOL = int;
alias UINT32 = uint;
alias uintptr_t = uint;


// ////////////////////////////////////////////////////////////////////////// //
// original source: blob.h in the mono sourcecode
// Encoding for type signatures used in the Metadata
alias MonoTypeEnum = uint;
enum /*MonoTypeEnum*/ : uint {
  MONO_TYPE_END        = 0x00, // End of List
  MONO_TYPE_VOID       = 0x01,
  MONO_TYPE_BOOLEAN    = 0x02,
  MONO_TYPE_CHAR       = 0x03,
  MONO_TYPE_I1         = 0x04,
  MONO_TYPE_U1         = 0x05,
  MONO_TYPE_I2         = 0x06,
  MONO_TYPE_U2         = 0x07,
  MONO_TYPE_I4         = 0x08,
  MONO_TYPE_U4         = 0x09,
  MONO_TYPE_I8         = 0x0a,
  MONO_TYPE_U8         = 0x0b,
  MONO_TYPE_R4         = 0x0c,
  MONO_TYPE_R8         = 0x0d,
  MONO_TYPE_STRING     = 0x0e,
  MONO_TYPE_PTR        = 0x0f, // arg: <type> token
  MONO_TYPE_BYREF      = 0x10, // arg: <type> token
  MONO_TYPE_VALUETYPE  = 0x11, // arg: <type> token
  MONO_TYPE_CLASS      = 0x12, // arg: <type> token
  MONO_TYPE_VAR        = 0x13, // number
  MONO_TYPE_ARRAY      = 0x14, // type, rank, boundsCount, bound1, loCount, lo1
  MONO_TYPE_GENERICINST= 0x15, // <type> <type-arg-count> <type-1> \x{2026} <type-n>
  MONO_TYPE_TYPEDBYREF = 0x16,
  MONO_TYPE_I          = 0x18,
  MONO_TYPE_U          = 0x19,
  MONO_TYPE_FNPTR      = 0x1b, // arg: full method signature
  MONO_TYPE_OBJECT     = 0x1c,
  MONO_TYPE_SZARRAY    = 0x1d, // 0-based one-dim-array
  MONO_TYPE_MVAR       = 0x1e, // number
  MONO_TYPE_CMOD_REQD  = 0x1f, // arg: typedef or typeref token
  MONO_TYPE_CMOD_OPT   = 0x20, // optional arg: typedef or typref token
  MONO_TYPE_INTERNAL   = 0x21, // CLR internal type

  MONO_TYPE_MODIFIER   = 0x40, // Or with the following types
  MONO_TYPE_SENTINEL   = 0x41, // Sentinel for varargs method signature
  MONO_TYPE_PINNED     = 0x45, // Local var that points to pinned object

  MONO_TYPE_ENUM       = 0x55, // an enumeration
}

alias MonoMetaTableEnum = uint;
enum /*MonoMetaTableEnum*/ : uint {
  MONO_TABLE_MODULE,
  MONO_TABLE_TYPEREF,
  MONO_TABLE_TYPEDEF,
  MONO_TABLE_FIELD_POINTER,
  MONO_TABLE_FIELD,
  MONO_TABLE_METHOD_POINTER,
  MONO_TABLE_METHOD,
  MONO_TABLE_PARAM_POINTER,
  MONO_TABLE_PARAM,
  MONO_TABLE_INTERFACEIMPL,
  MONO_TABLE_MEMBERREF, // 0xa
  MONO_TABLE_CONSTANT,
  MONO_TABLE_CUSTOMATTRIBUTE,
  MONO_TABLE_FIELDMARSHAL,
  MONO_TABLE_DECLSECURITY,
  MONO_TABLE_CLASSLAYOUT,
  MONO_TABLE_FIELDLAYOUT, // 0x10
  MONO_TABLE_STANDALONESIG,
  MONO_TABLE_EVENTMAP,
  MONO_TABLE_EVENT_POINTER,
  MONO_TABLE_EVENT,
  MONO_TABLE_PROPERTYMAP,
  MONO_TABLE_PROPERTY_POINTER,
  MONO_TABLE_PROPERTY,
  MONO_TABLE_METHODSEMANTICS,
  MONO_TABLE_METHODIMPL,
  MONO_TABLE_MODULEREF, // 0x1a
  MONO_TABLE_TYPESPEC,
  MONO_TABLE_IMPLMAP,
  MONO_TABLE_FIELDRVA,
  MONO_TABLE_UNUSED6,
  MONO_TABLE_UNUSED7,
  MONO_TABLE_ASSEMBLY, // 0x20
  MONO_TABLE_ASSEMBLYPROCESSOR,
  MONO_TABLE_ASSEMBLYOS,
  MONO_TABLE_ASSEMBLYREF,
  MONO_TABLE_ASSEMBLYREFPROCESSOR,
  MONO_TABLE_ASSEMBLYREFOS,
  MONO_TABLE_FILE,
  MONO_TABLE_EXPORTEDTYPE,
  MONO_TABLE_MANIFESTRESOURCE,
  MONO_TABLE_NESTEDCLASS,
  MONO_TABLE_GENERICPARAM, // 0x2a
  MONO_TABLE_METHODSPEC,
  MONO_TABLE_GENERICPARAMCONSTRAINT,
}

enum {
  MONO_TYPEDEF_FLAGS,
  MONO_TYPEDEF_NAME,
  MONO_TYPEDEF_NAMESPACE,
  MONO_TYPEDEF_EXTENDS,
  MONO_TYPEDEF_FIELD_LIST,
  MONO_TYPEDEF_METHOD_LIST,
  MONO_TYPEDEF_SIZE
}

enum {
  MONO_METHOD_RVA,
  MONO_METHOD_IMPLFLAGS,
  MONO_METHOD_FLAGS,
  MONO_METHOD_NAME,
  MONO_METHOD_SIGNATURE,
  MONO_METHOD_PARAMLIST,
  MONO_METHOD_SIZE
}

alias MonoTokenType = uint;
enum /*MonoTokenType*/ : uint {
  MONO_TOKEN_MODULE            = 0x00000000,
  MONO_TOKEN_TYPE_REF          = 0x01000000,
  MONO_TOKEN_TYPE_DEF          = 0x02000000,
  MONO_TOKEN_FIELD_DEF         = 0x04000000,
  MONO_TOKEN_METHOD_DEF        = 0x06000000,
  MONO_TOKEN_PARAM_DEF         = 0x08000000,
  MONO_TOKEN_INTERFACE_IMPL    = 0x09000000,
  MONO_TOKEN_MEMBER_REF        = 0x0a000000,
  MONO_TOKEN_CUSTOM_ATTRIBUTE  = 0x0c000000,
  MONO_TOKEN_PERMISSION        = 0x0e000000,
  MONO_TOKEN_SIGNATURE         = 0x11000000,
  MONO_TOKEN_EVENT             = 0x14000000,
  MONO_TOKEN_PROPERTY          = 0x17000000,
  MONO_TOKEN_MODULE_REF        = 0x1a000000,
  MONO_TOKEN_TYPE_SPEC         = 0x1b000000,
  MONO_TOKEN_ASSEMBLY          = 0x20000000,
  MONO_TOKEN_ASSEMBLY_REF      = 0x23000000,
  MONO_TOKEN_FILE              = 0x26000000,
  MONO_TOKEN_EXPORTED_TYPE     = 0x27000000,
  MONO_TOKEN_MANIFEST_RESOURCE = 0x28000000,
  MONO_TOKEN_GENERIC_PARAM     = 0x2a000000,
  MONO_TOKEN_METHOD_SPEC       = 0x2b000000,

  // These do not match metadata tables directly
  MONO_TOKEN_STRING            = 0x70000000,
  MONO_TOKEN_NAME              = 0x71000000,
  MONO_TOKEN_BASE_TYPE         = 0x72000000,
}


// ////////////////////////////////////////////////////////////////////////// //
alias MonoDomainFunc = extern(C) void function (void* domain, void* user_data) nothrow @nogc;
alias GFunc = extern(C) void function (void* data, void* user_data) nothrow @nogc;

alias G_FREE = extern(C) void function (void* ptr) nothrow @nogc;

alias MONO_GET_ROOT_DOMAIN = extern(C) void* function () nothrow @nogc;
alias MONO_THREAD_ATTACH = extern(C) void* function (void* domain) nothrow @nogc;
alias MONO_THREAD_DETACH = extern(C) void function (void* monothread) nothrow @nogc;
alias MONO_OBJECT_GET_CLASS = extern(C) void* function (void* object) nothrow @nogc;

alias MONO_DOMAIN_FOREACH = extern(C) void function (MonoDomainFunc func, void* user_data) nothrow /*@nogc*/;

alias MONO_DOMAIN_SET = extern(C) int function (void* domain, BOOL force) nothrow @nogc;
alias MONO_ASSEMBLY_FOREACH = extern(C) int function (GFunc func, void* user_data) nothrow /*@nogc*/;
alias MONO_ASSEMBLY_GET_IMAGE = extern(C) void* function (void* assembly) nothrow @nogc;
alias MONO_ASSEMBLY_OPEN = extern(C) void* function (void* fname, int* status) nothrow @nogc;
alias MONO_IMAGE_GET_ASSEMBLY = extern(C) void* function (void* image) nothrow @nogc;
alias MONO_IMAGE_GET_NAME = extern(C) char* function (void* image) nothrow @nogc;
alias MONO_IMAGE_OPEN = extern(C) void* function (const(char)* fname, int* status) nothrow @nogc;

alias MONO_IMAGE_GET_TABLE_INFO = extern(C) void* function (void* image, int table_id) nothrow @nogc;
alias MONO_TABLE_INFO_GET_ROWS = extern(C) int function (void* tableinfo) nothrow @nogc;
alias MONO_METADATA_DECODE_ROW_COL = extern(C) int function (void* tableinfo, int idx, uint col) nothrow @nogc;
alias MONO_METADATA_STRING_HEAP = extern(C) ubyte* function (void* image, UINT32 index) nothrow @nogc; //???

alias MONO_CLASS_FROM_NAME_CASE = extern(C) void* function (void* image, const(char)* name_space, const(char)* name) nothrow @nogc;
alias MONO_CLASS_GET_NAME = extern(C) char* function (void* klass) nothrow @nogc;
alias MONO_CLASS_GET_NAMESPACE = extern(C) char* function (void* klass) nothrow @nogc; // returns namespace name
alias MONO_CLASS_GET = extern(C) void* function (void* image, UINT32 tokenindex) nothrow @nogc;
alias MONO_CLASS_GET_METHODS = extern(C) void* function (void* klass, void* iter) nothrow @nogc;
alias MONO_CLASS_GET_METHOD_FROM_NAME = extern(C) void* function (void* klass, const(char)* methodname, int paramcount) nothrow @nogc;
alias MONO_CLASS_GET_FIELDS = extern(C) void* function (void* klass, void* iter) nothrow @nogc;
alias MONO_CLASS_GET_PARENT = extern(C) void* function (void* klass) nothrow @nogc;
alias MONO_CLASS_VTABLE = extern(C) void* function (void* domain, void* klass) nothrow @nogc;
alias MONO_CLASS_FROM_MONO_TYPE = extern(C) void* function (void* type) nothrow @nogc;
alias MONO_CLASS_GET_ELEMENT_CLASS = extern(C) void* function (void* klass) nothrow @nogc;

alias MONO_CLASS_NUM_FIELDS = extern(C) int function (void* klass) nothrow @nogc;
alias MONO_CLASS_NUM_METHODS = extern(C) int function (void* klass) nothrow @nogc;

alias MONO_FIELD_GET_NAME = extern(C) char* function (void* field) nothrow @nogc; // should be g_free'd
alias MONO_FIELD_GET_TYPE = extern(C) void* function (void* field) nothrow @nogc;
alias MONO_FIELD_GET_PARENT = extern(C) void* function (void* field) nothrow @nogc;
alias MONO_FIELD_GET_OFFSET = extern(C) int function (void* field) nothrow @nogc;

alias MONO_TYPE_GET_NAME = extern(C) char* function (void* type) nothrow @nogc;
alias MONO_TYPE_GET_TYPE = extern(C) int function (void* type) nothrow @nogc;
alias MONO_TYPE_GET_NAME_FULL = extern(C) char* function (void* type, int format) nothrow @nogc;
alias MONO_FIELD_GET_FLAGS = extern(C) int function (void* type) nothrow @nogc;

alias MONO_METHOD_GET_NAME = extern(C) char* function (void* method) nothrow @nogc;
alias MONO_COMPILE_METHOD = extern(C) void* function (void* method) nothrow @nogc;
alias MONO_FREE_METHOD = extern(C) void function (void* method) nothrow @nogc;

alias MONO_JIT_INFO_TABLE_FIND = extern(C) void* function (void* domain, void* addr) nothrow @nogc;

alias MONO_JIT_INFO_GET_METHOD = extern(C) void* function (void* jitinfo) nothrow @nogc;
alias MONO_JIT_INFO_GET_CODE_START = extern(C) void* function (void* jitinfo) nothrow @nogc;
alias MONO_JIT_INFO_GET_CODE_SIZE = extern(C) int function (void* jitinfo) nothrow @nogc;

alias MONO_METHOD_GET_HEADER = extern(C) void* function (void* method) nothrow @nogc;
alias MONO_METHOD_GET_CLASS = extern(C) void* function (void* method) nothrow @nogc;
alias MONO_METHOD_SIG = extern(C) void* function (void* method) nothrow @nogc;
alias MONO_METHOD_GET_PARAM_NAMES = extern(C) void* function (void* method, const(char)** names) nothrow @nogc;

alias MONO_METHOD_HEADER_GET_CODE = extern(C) void* function (void* methodheader, UINT32 *code_size, UINT32 *max_stack) nothrow @nogc;
alias MONO_DISASM_CODE = extern(C) char* function (void* dishelper, void* method, void* ip, void* end) nothrow @nogc;

alias MONO_SIGNATURE_GET_DESC = extern(C) char* function (void* signature, int include_namespace) nothrow @nogc;
alias MONO_SIGNATURE_GET_PARAM_COUNT = extern(C) int function (void* signature) nothrow @nogc;
alias MONO_SIGNATURE_GET_RETURN_TYPE = extern(C) void* function (void* signature) nothrow @nogc;

alias MONO_IMAGE_RVA_MAP = extern(C) void* function (void* image, UINT32 addr) nothrow @nogc;
alias MONO_VTABLE_GET_STATIC_FIELD_DATA = extern(C) void* function (void* vtable) nothrow @nogc;

alias MONO_METHOD_DESC_NEW = extern(C) void* function (const(char)* name, int include_namespace) nothrow @nogc;
alias MONO_METHOD_DESC_FROM_METHOD = extern(C) void* function (void* method) nothrow @nogc;
alias MONO_METHOD_DESC_FREE = extern(C) void function (void* desc) nothrow @nogc;

alias MONO_ASSEMBLY_NAME_NEW = extern(C) void* function (const(char)* name) nothrow @nogc;
alias MONO_ASSEMBLY_LOADED = extern(C) void* function (void* aname) nothrow @nogc;
alias MONO_IMAGE_LOADED = extern(C) void* function (void* aname) nothrow @nogc;

alias MONO_STRING_NEW = extern(C) void* function (void* domain, const(char)* text) nothrow @nogc;
alias MONO_STRING_TO_UTF8 = extern(C) char* function (void*) nothrow @nogc;
alias MONO_ARRAY_NEW = extern(C) void* function (void* domain, void* eclass, uintptr_t n) nothrow @nogc;
alias MONO_OBJECT_TO_STRING = extern(C) void* function (void* object, void* *exc) nothrow @nogc;
alias MONO_FREE = extern(C) void function (void*) nothrow @nogc;

alias MONO_METHOD_DESC_SEARCH_IN_IMAGE = extern(C) void* function (void* desc, void* image) nothrow @nogc;
alias MONO_RUNTIME_INVOKE = extern(C) void* function (void* method, void* obj, void* *params, void* *exc) nothrow @nogc;
alias MONO_RUNTIME_INVOKE_ARRAY = extern(C) void* function (void* method, void* obj, void* params, void* *exc) nothrow @nogc;
alias MONO_VALUE_BOX = extern(C) void* function (void* domain, void* klass, void* val) nothrow @nogc;
alias MONO_OBJECT_UNBOX = extern(C) void* function (void* obj) nothrow @nogc;
alias MONO_CLASS_GET_TYPE = extern(C) void* function (void* klass) nothrow @nogc;


// ////////////////////////////////////////////////////////////////////////// //
enum MonoAPI;
enum Optional;

__gshared void* hmono;
__gshared void* mono_selfthread;

@MonoAPI G_FREE g_free;
@MonoAPI __gshared MONO_GET_ROOT_DOMAIN mono_get_root_domain;
@MonoAPI __gshared MONO_THREAD_ATTACH mono_thread_attach;
@MonoAPI __gshared MONO_THREAD_DETACH mono_thread_detach;
@MonoAPI __gshared MONO_OBJECT_GET_CLASS mono_object_get_class;
@MonoAPI __gshared MONO_CLASS_GET_NAME mono_class_get_name;
@MonoAPI __gshared MONO_CLASS_GET_NAMESPACE mono_class_get_namespace;
@MonoAPI __gshared MONO_CLASS_GET_PARENT mono_class_get_parent;
@MonoAPI __gshared MONO_CLASS_VTABLE mono_class_vtable;
@MonoAPI __gshared MONO_CLASS_FROM_MONO_TYPE mono_class_from_mono_type;

@MonoAPI __gshared MONO_DOMAIN_FOREACH mono_domain_foreach;
@MonoAPI __gshared MONO_DOMAIN_SET mono_domain_set;
@MonoAPI __gshared MONO_ASSEMBLY_FOREACH mono_assembly_foreach;
@MonoAPI __gshared MONO_ASSEMBLY_GET_IMAGE mono_assembly_get_image;
@MonoAPI __gshared MONO_IMAGE_GET_ASSEMBLY mono_image_get_assembly;
@MonoAPI __gshared MONO_ASSEMBLY_OPEN mono_assembly_open;

@MonoAPI __gshared MONO_IMAGE_GET_NAME mono_image_get_name;
@MonoAPI __gshared MONO_IMAGE_GET_TABLE_INFO mono_image_get_table_info;
@MonoAPI __gshared MONO_IMAGE_RVA_MAP mono_image_rva_map;
@MonoAPI __gshared MONO_IMAGE_OPEN mono_image_open;
@MonoAPI __gshared MONO_IMAGE_LOADED mono_image_loaded;

@MonoAPI __gshared MONO_TABLE_INFO_GET_ROWS mono_table_info_get_rows;
@MonoAPI __gshared MONO_METADATA_DECODE_ROW_COL mono_metadata_decode_row_col;
@MonoAPI __gshared MONO_METADATA_STRING_HEAP mono_metadata_string_heap;
@MonoAPI __gshared MONO_CLASS_GET mono_class_get;
@MonoAPI __gshared MONO_CLASS_FROM_NAME_CASE mono_class_from_name_case;

@MonoAPI __gshared MONO_CLASS_NUM_FIELDS mono_class_num_fields;
@MonoAPI __gshared MONO_CLASS_GET_FIELDS mono_class_get_fields;

@MonoAPI __gshared MONO_CLASS_NUM_METHODS mono_class_num_methods;
@MonoAPI __gshared MONO_CLASS_GET_METHODS mono_class_get_methods;

@MonoAPI __gshared MONO_CLASS_GET_METHOD_FROM_NAME mono_class_get_method_from_name;
@MonoAPI __gshared MONO_CLASS_GET_ELEMENT_CLASS mono_class_get_element_class;


@MonoAPI __gshared MONO_FIELD_GET_NAME mono_field_get_name;
@MonoAPI __gshared MONO_FIELD_GET_TYPE mono_field_get_type;
@MonoAPI __gshared MONO_FIELD_GET_PARENT mono_field_get_parent;
@MonoAPI __gshared MONO_FIELD_GET_OFFSET mono_field_get_offset;

@MonoAPI __gshared MONO_TYPE_GET_NAME mono_type_get_name;
@MonoAPI __gshared MONO_TYPE_GET_TYPE mono_type_get_type;
@MonoAPI __gshared MONO_TYPE_GET_NAME_FULL mono_type_get_name_full;
@MonoAPI __gshared MONO_FIELD_GET_FLAGS mono_field_get_flags;

@MonoAPI __gshared MONO_METHOD_GET_NAME mono_method_get_name;
@MonoAPI __gshared MONO_METHOD_GET_HEADER mono_method_get_header;
@MonoAPI __gshared MONO_METHOD_GET_CLASS mono_method_get_class;
@MonoAPI __gshared MONO_METHOD_SIG mono_method_signature;
@MonoAPI __gshared MONO_METHOD_GET_PARAM_NAMES mono_method_get_param_names;

@MonoAPI __gshared MONO_SIGNATURE_GET_DESC mono_signature_get_desc;
@MonoAPI __gshared MONO_SIGNATURE_GET_PARAM_COUNT mono_signature_get_param_count;
@MonoAPI __gshared MONO_SIGNATURE_GET_RETURN_TYPE mono_signature_get_return_type;


@MonoAPI __gshared MONO_COMPILE_METHOD mono_compile_method;
@MonoAPI __gshared MONO_FREE_METHOD mono_free_method;

@MonoAPI __gshared MONO_JIT_INFO_TABLE_FIND mono_jit_info_table_find;
@MonoAPI __gshared MONO_JIT_INFO_GET_METHOD mono_jit_info_get_method;
@MonoAPI __gshared MONO_JIT_INFO_GET_CODE_START mono_jit_info_get_code_start;
@MonoAPI __gshared MONO_JIT_INFO_GET_CODE_SIZE mono_jit_info_get_code_size;

@MonoAPI __gshared MONO_METHOD_HEADER_GET_CODE mono_method_header_get_code;
@MonoAPI __gshared MONO_DISASM_CODE mono_disasm_code;

@MonoAPI __gshared MONO_VTABLE_GET_STATIC_FIELD_DATA mono_vtable_get_static_field_data;

@MonoAPI __gshared MONO_METHOD_DESC_NEW mono_method_desc_new;
@MonoAPI __gshared MONO_METHOD_DESC_FROM_METHOD mono_method_desc_from_method;
@MonoAPI __gshared MONO_METHOD_DESC_FREE mono_method_desc_free;
@MonoAPI @Optional __gshared MONO_ASSEMBLY_NAME_NEW mono_assembly_name_new;
@MonoAPI __gshared MONO_ASSEMBLY_LOADED mono_assembly_loaded;

@MonoAPI __gshared MONO_STRING_NEW mono_string_new;
@MonoAPI __gshared MONO_STRING_TO_UTF8 mono_string_to_utf8;
@MonoAPI __gshared MONO_ARRAY_NEW mono_array_new;
@MonoAPI @Optional __gshared MONO_OBJECT_TO_STRING mono_object_to_string;
@MonoAPI @Optional __gshared MONO_FREE mono_free; //???
@MonoAPI __gshared MONO_VALUE_BOX mono_value_box;
@MonoAPI __gshared MONO_OBJECT_UNBOX mono_object_unbox;
@MonoAPI __gshared MONO_CLASS_GET_TYPE mono_class_get_type;

@MonoAPI __gshared MONO_METHOD_DESC_SEARCH_IN_IMAGE mono_method_desc_search_in_image;
@MonoAPI __gshared MONO_RUNTIME_INVOKE mono_runtime_invoke;

__gshared int attached = 0;


// ////////////////////////////////////////////////////////////////////////// //
__gshared void* k32h;
extern(Windows) __gshared void* function (const(char)* name) nothrow @nogc LoadLibA;
extern(Windows) __gshared void* function (void* hmod, const(char)* name) nothrow @nogc GetProcAddr;


string getKernelDllName () {
  import core.sys.posix.unistd : getpid;
  import core.stdc.stdio : FILE, fopen, fclose, snprintf, fgets;
  import core.stdc.string : strstr;
  bool found = false;
  FILE *fp;
  char[30] filename;
  char[852] line;
  int pid = cast(int)getpid();
  snprintf(filename.ptr, filename.length, "/proc/%d/maps", pid);
  fp = fopen(filename.ptr, "r");
  if (fp is null) return null;
  while (fgets(line.ptr, 850, fp) !is null) {
    import std.string : fromStringz;
    auto ln = line.ptr.fromStringz;
    while (ln.length && ln[$-1] <= ' ') ln = ln[0..$-1];
    if (ln.length == 0 || ln.ptr[0] <= ' ') continue;
    // remove 5 fields
    foreach (immutable _; 0..5) {
      while (ln.length > 0 && ln.ptr[0] > ' ') ln = ln[1..$];
      while (ln.length > 0 && ln.ptr[0] <= ' ') ln = ln[1..$];
    }
    if (ln.length == 0) continue;
    auto savedln = ln;
    int pos = cast(int)ln.length;
    while (pos > 0 && ln.ptr[pos-1] != '/') --pos;
    ln = ln[pos..$];
    if (ln == "kernel32.dll.so") {
      // add zero (it is safe, we always has room for it)
      savedln.ptr[savedln.length] = 0;
      auto dp = new char[](savedln.length+1);
      dp[] = savedln.ptr[0..savedln.length+1];
      return cast(string)(dp[0..$-1]); // zero is there, but not included in length
    }
  }
  fclose(fp);
  return null;
}


// ////////////////////////////////////////////////////////////////////////// //
public void loadWineAPI () {
  import core.stdc.stdio : stderr, fprintf;

  auto k32name = getKernelDllName();
  if (k32name.length == 0) { fprintf(stderr, "MSG: not a WINE process!\n"); return; }
  fprintf(stderr, "MSG: kernel: [%s]\n", k32name.ptr);

  k32h = __libc_dlopen_mode(k32name.ptr, 1); // lazy
  if (k32h is null) { fprintf(stderr, "MSG: FUCK0 (no kernel32.dll)\n"); return; }

  *cast(void**)&LoadLibA = __libc_dlsym(k32h, "LoadLibraryA");
  if (LoadLibA is null) { fprintf(stderr, "MSG: FUCK1 (no LoadLibraryA)\n"); k32h = null; return; }

  *cast(void**)&GetProcAddr = __libc_dlsym(k32h, "GetProcAddress");
  if (GetProcAddr is null) { fprintf(stderr, "MSG: FUCK2 (no GetProcAddress)\n"); k32h = null; return; }

  fprintf(stderr, "MSG: WINE API loaded\n");
}

public @property bool isWineAPILoaded () nothrow @trusted @nogc { return (k32h !is null); }


// ////////////////////////////////////////////////////////////////////////// //
public void loadMonoAPI () {
  import core.stdc.stdio : stderr, fprintf;
  import std.traits;

  if (!isWineAPILoaded) {
    fprintf(stderr, "MSG: no WINE API was found\n");
    attached = -1;
    return;
  }

  if (attached != 0) return;

  hmono = LoadLibA("mono.dll");
  if (hmono is null) {
    fprintf(stderr, "MSG: no mono API was found\n");
    attached = -1;
    return;
  }

  attached = 1;
  foreach (string memname; __traits(allMembers, mixin(__MODULE__))) {
    static if (is(typeof(__traits(getMember, mixin(__MODULE__), memname)))) {
      alias mem = Identity!(__traits(getMember, mixin(__MODULE__), memname));
      static if (is(typeof(&mem)) && hasUDA!(mem, MonoAPI)) {
        //pragma(msg, memname);
        mixin("*cast(void**)&"~memname~" = GetProcAddr(hmono, "~memname.stringof~");");
        static if (hasUDA!(mem, Optional)) {
          mixin("if ("~memname~` is null) { import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "MSG: mono API '%s' not found!\n", `~memname.stringof~".ptr); }");
        } else {
          mixin("if ("~memname~` is null) { import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "MSG: mono API '%s' not found (FATAL)!\n", `~memname.stringof~".ptr); }");
          mixin("if ("~memname~" is null) attached = -1;");
        }
      }
    }
  }
  if (attached == 1) mono_selfthread = mono_thread_attach(mono_get_root_domain());
}

public @property bool isMonoAPILoaded () nothrow @trusted @nogc { return (attached == 1); }


// ////////////////////////////////////////////////////////////////////////// //
public string getAssemblyNameEP (uint asmid) {
  auto image = mono_assembly_get_image(cast(void*)asmid);
  if (image is null) return "<none>";
  auto imgname = mono_image_get_name(image);
  if (imgname is null) return "<none>";
  scope(exit) g_free(imgname);
  return imgname.fromStringz.idup;
}


// ////////////////////////////////////////////////////////////////////////// //
extern(C) void assemblyEnumerator (void* assembly, void* userptr) nothrow {
  auto image = mono_assembly_get_image(assembly);
  if (image is null) return;
  auto imgname = mono_image_get_name(image);
  if (imgname is null) return;
  scope(exit) g_free(imgname);
  AssemblyInfo[]* arr = cast(AssemblyInfo[]*)userptr;
  try {
    (*arr) ~= AssemblyInfo(cast(uint)assembly, imgname.fromStringz.idup);
  } catch (Exception) {}
}

public AssemblyInfo[] enumAssembliesEP () {
  AssemblyInfo[] arr;
  mono_assembly_foreach(cast(GFunc)&assemblyEnumerator, &arr);
  return arr;
}


// ////////////////////////////////////////////////////////////////////////// //
void collectAssemblyClasses (uint asmid, ref NCClassInfo[] clist) {
  auto image = mono_assembly_get_image(cast(void*)asmid);
  if (image is null) return;
  //auto imgname = mono_image_get_name(image);
  //if (imgname is null) return;
  //scope(exit) g_free(imgname);
  //auto imgnamestr = imgname.fromStringz.idup;
  auto tdef = mono_image_get_table_info(image, MONO_TABLE_TYPEDEF);
  if (tdef is null) return;
  auto tdefcount = mono_table_info_get_rows(tdef);
  foreach (immutable int i; 0..tdefcount) {
    auto c = mono_class_get(image, MONO_TOKEN_TYPE_DEF|i+1);
    auto name = mono_class_get_name(c);
    if (name is null) continue;
    scope(exit) g_free(name);
    auto nspace = mono_class_get_namespace(c);
    scope(exit) if (nspace !is null) g_free(nspace);
    clist ~= NCClassInfo(cast(uint)c, asmid, name.fromStringz.idup, nspace.fromStringz.idup);
  }
}

extern(C) void assemblyEnumeratorForClassList (void* assembly, void* userptr) nothrow {
  auto image = mono_assembly_get_image(assembly);
  if (image is null) return;
  NCClassInfo[]* arr = cast(NCClassInfo[]*)userptr;
  try {
    collectAssemblyClasses(cast(uint)assembly, *arr);
  } catch (Exception) {}
}

public NCClassInfo[] getClassListEP (uint asmid=0) {
  NCClassInfo[] arr;
  if (asmid == 0) {
    mono_assembly_foreach(cast(GFunc)&assemblyEnumeratorForClassList, &arr);
  } else {
    collectAssemblyClasses(asmid, arr);
  }
  return arr;
}


// ////////////////////////////////////////////////////////////////////////// //
public FieldInfo[] getClassFieldsEP (uint clsid) {
  void* c = cast(void*)clsid;
  auto cname = mono_class_get_name(c);
  if (cname is null) return null;
  scope(exit) g_free(cname);
  void* iter = null;
  FieldInfo[] res;
  for (;;) {
    auto field = mono_class_get_fields(c, &iter);
    if (field is null) break;
    auto fieldtype = mono_field_get_type(field);
    auto name = mono_field_get_name(field);
    if (name is null) continue;
    scope(exit) g_free(name);
    auto tname = mono_type_get_name(fieldtype);
    if (tname is null) continue;
    scope(exit) g_free(tname);
    FieldInfo fi;
    fi.type = mono_type_get_type(fieldtype);
    fi.ofs = mono_field_get_offset(field);
    fi.flags = mono_field_get_flags(field);
    fi.name = name.fromStringz.idup;
    fi.typename = tname.fromStringz.idup;
    /*
    WriteQword((UINT_PTR)mono_field_get_parent(field));
    WriteDword(mono_field_get_flags(field));
    */
    res ~= fi;
  }
  return res;
}
