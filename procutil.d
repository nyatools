/* Written by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module procutil is aliced;

import iv.olly.assembler;
import iv.vfs;
import iv.vfs.io;

version = pt_use_mem;
static assert((void*).sizeof == 4);


// ////////////////////////////////////////////////////////////////////////// //
// pid or -1
int findProcessByName (const(char)[] procname) {
  import core.sys.posix.dirent;
  static assert(dirent.d_name.offsetof == 19);
  import core.sys.posix.unistd : readlink;
  import core.stdc.stdio : snprintf;
  import std.conv : to;
  import std.string : fromStringz;

  if (procname.length == 0) return -1;

  char[128] tbuf = void;
  char[1024] ebuf = void;

  auto dir = opendir("/proc/");
  if (dir is null) return -1;
  scope(exit) closedir(dir);

  for (;;) {
    auto de = readdir(dir);
    if (de is null) break;
    if (de.d_type != DT_DIR) continue;

    int pid = 0;

    try {
      pid = de.d_name.ptr.fromStringz.to!int;
    } catch (Exception) { pid = 0; }

    if (pid > 1 && pid <= int.max) {
      snprintf(tbuf.ptr, tbuf.length, "/proc/%s/exe", de.d_name.ptr);
      tbuf[$-1] = 0;
      auto len = readlink(tbuf.ptr, ebuf.ptr, ebuf.length);
      if (len > 0 && len < ebuf.length) {
        //{ import core.stdc.stdio : printf; ebuf[len] = 0; printf("pid: %u; exe: [%s]\n", pid, ebuf.ptr); }
        if (ebuf[0..len] == procname) return pid;
        int pos = cast(int)len;
        while (pos > 0 && ebuf[pos-1] != '/') --pos;
        if (ebuf[pos..len] == procname) return pid;
        if (ebuf[pos..len] == "wine-preloader") {
          // oops, wine app; get name from command line
          import core.stdc.stdio : FILE, fopen, fclose, fread;
          snprintf(tbuf.ptr, tbuf.length, "/proc/%s/cmdline", de.d_name.ptr);
          tbuf[$-1] = 0;
          auto fl = fopen(tbuf.ptr, "r");
          if (fl !is null) {
            scope(exit) fclose(fl);
            char[850] line;
            len = fread(line.ptr, 1, line.length-1, fl);
            if (len > 0) {
              line.ptr[len] = 0;
              //{ import core.stdc.stdio; printf("%u: lx=[%s]\n", pid, line.ptr); }
              pos = 0;
              while (pos < len && line.ptr[pos]) ++pos;
              auto lx = line[0..pos];
              //{ import std.stdio; writeln("<", lx, ">"); }
              if (lx == procname) return pid;
              pos = cast(int)lx.length;
              while (pos > 0 && lx[pos-1] != '/' && lx[pos-1] != '\\') --pos;
              if (lx[pos..$] == procname) return pid;
            }
          }
        }
      }
    }
  }

  return -1;
}


// ////////////////////////////////////////////////////////////////////////// //
public align(1) struct MemoryRegionInfo {
align(1):
  uint start, end;
  string name;
}


// ////////////////////////////////////////////////////////////////////////// //
public MemoryRegionInfo[] readMemoryRegions (uint pid, bool includeStack=false) {
  import std.format : format;

  static byte hexDigit (char ch) pure nothrow @safe @nogc {
    pragma(inline, true);
    return
      ch >= '0' && ch <= '9' ? cast(byte)(ch-'0') :
      ch >= 'A' && ch <= 'F' ? cast(byte)(ch-'A'+10) :
      ch >= 'a' && ch <= 'f' ? cast(byte)(ch-'a'+10) :
      -1;
  }

  static uint parseHex(T : const(char)[]) (ref T s) {
    while (s.length > 0 && s.ptr[0] <= ' ') s = s[1..$];
    if (s.length == 0 || hexDigit(s.ptr[0]) < 0) throw new Exception("hex number expected");
    uint res = 0;
    while (s.length) {
      auto d = hexDigit(s.ptr[0]);
      if (d < 0) break;
      uint nr = res*16+d;
      if (nr < res) throw new Exception("hex overflow");
      res = nr;
      s = s[1..$];
    }
    return res;
  }

  static void consume(T : const(char)[]) (ref T s, char ch) {
    if (s.length == 0 && s.ptr[0] != ch) throw new Exception("'"~ch~"' expected");
    s = s[1..$];
  }

  static void skipSpaces(T : const(char)[]) (ref T s) {
    if (s.length == 0 && s.ptr[0] > ' ') throw new Exception("space expected");
    while (s.length > 0 && s.ptr[0] <= ' ') s = s[1..$];
  }

  MemoryRegionInfo[] res;
  foreach (char[] ln; VFile("/proc/%s/maps".format(pid)).byLine) {
    uint start, end;
    string name;
    try {
      //stderr.writeln("*** [", ln, "]");
      start = parseHex(ln);
      consume(ln, '-');
      end = parseHex(ln);
      skipSpaces(ln);
      if (start >= end) continue;
      // skip regions with less than 16 bytes of data
      if (end-start < 16) continue;
      if (ln.length < 5) throw new Exception("invalid region description");
      // skip non-writeable, {executable} and shared regions
      if (ln.ptr[0] != 'r' || ln.ptr[1] != 'w' /*|| ln.ptr[2] == 'x'*/ || ln.ptr[3] != 'p') continue;
      ln = ln[4..$];
      skipSpaces(ln);
      // if offset is not 0, it is mmaped region, skip it
      if (parseHex(ln) != 0) continue;
      // if devicehi or devilelo is not 0, it is mmaped region, skip it
      skipSpaces(ln);
      if (parseHex(ln) != 0) continue;
      consume(ln, ':');
      if (parseHex(ln) != 0) continue;
      // if inode is not 0, it is mmaped region, skip it
      skipSpaces(ln);
      if (parseHex(ln) != 0) continue;
      // get region name
      if (ln.length > 0) {
        skipSpaces(ln);
        name = ln.idup;
        while (name.length && name[$-1] <= ' ') name = name[0..$-1];
      }
    } catch (Exception) {
      // can't parse this region, skip it
      continue;
    }
    if (name.length > 0 && name.ptr[0] == '[') {
      // specials; allow only "heap"
      //writefln("**** %08X:%08X  <%s>", start, end, name);
      if (name.length < 6 || name[0..5] != "[heap") {
        if (!includeStack) continue;
        if (name.length < 6 || name[0..6] != "[stack") continue;
      }
    }
    //stderr.writefln("%08X:%08X  <%s>", start, end, name);
    res ~= MemoryRegionInfo(start, end, name);
  }
  return res;
}


// ////////////////////////////////////////////////////////////////////////// //
/*
 * search the target process' /proc/pid/maps entry and find an executable
 * region of memory that we can use to run code in.
 * returns zero if nothing was found.
 */
public uint findXSpaceInPrey (int pid) {
  // try to get "ld-" first
  if (auto lda = findLibInPrey(pid, "ld-")) return lda;
  import core.stdc.stdio : FILE, fopen, fclose, snprintf, fgets, sscanf;
  import core.stdc.string : strstr;
  bool found = false;
  FILE* fp;
  char[30] filename;
  char[850] line;
  c_ulong addr, eaddr;
  char[16] perms;
  snprintf(filename.ptr, filename.length, "/proc/%u/maps", cast(uint)pid);
  fp = fopen(filename.ptr, "r");
  if (fp is null) return 0;
  while (fgets(line.ptr, 850, fp) !is null) {
    sscanf(line.ptr, "%lx-%lx %s %*s %*s %*d", &addr, &eaddr, perms.ptr);
    if (strstr(perms.ptr, "x") !is null && eaddr > addr && eaddr-addr >= 512) { found = true; break; }
  }
  fclose(fp);
  return (found ? addr : 0);
}


/*
 * gets the base address of libc.so inside a process by reading /proc/pid/maps.
 * returns 0 if nothing was found.
 * basename is name without version and .so, i.e. "libc-" for libc, for example
 */
private uint findLibInPrey (int pid, const(char)[] basename) {
  import core.stdc.stdio : FILE, fopen, fclose, snprintf, fgets, sscanf;
  import core.stdc.string : strstr;
  bool found = false;
  FILE *fp;
  char[30] filename;
  char[850] line;
  c_ulong addr;
  char[16] perms;
  snprintf(filename.ptr, filename.length, "/proc/%d/maps", pid);
  fp = fopen(filename.ptr, "r");
  if (fp is null) return 0;
  while (fgets(line.ptr, 850, fp) !is null) {
    import std.string : fromStringz;
    auto ln = line.ptr.fromStringz;
    while (ln.length && ln[$-1] <= ' ') ln = ln[0..$-1];
    if (ln.length == 0 || ln.ptr[0] <= ' ') continue;
    // remove 5 fields
    foreach (immutable _; 0..5) {
      while (ln.length > 0 && ln.ptr[0] > ' ') ln = ln[1..$];
      while (ln.length > 0 && ln.ptr[0] <= ' ') ln = ln[1..$];
    }
    if (ln.length == 0) continue;
    int pos = cast(int)ln.length;
    while (pos > 0 && ln.ptr[pos-1] != '/') --pos;
    ln = ln[pos..$];
    if (ln.length < basename.length+3 || ln[0..basename.length] != basename) continue;
    ln = ln[basename.length..$];
    pos = 0;
    // digits and dots
    while (pos < ln.length) {
      if (ln.ptr[pos] >= '0' && ln.ptr[pos] <= '9') { ++pos; continue; }
      if (ln.ptr[pos] != '.') break;
      if (ln.length-pos > 1 && ln.ptr[pos+1] >= '0' && ln.ptr[pos+1] <= '9') { pos += 2; continue; }
      break;
    }
    //{ import std.stdio; stderr.writeln("[", ln, "] - [", ln[pos..$], "]"); }
    if (pos >= ln.length || ln.ptr[pos] != '.' || ln.length-pos < 3 || ln[pos..pos+3] != ".so") continue;
    ln = ln[pos+3..$];
    //{ import std.stdio; stderr.writeln("*[", ln, "]"); }
    if (ln.length != 0) {
      // it should be dots and digits
      if (ln.ptr[0] != '.') continue;
      pos = 0;
      while (pos < ln.length) {
        if (ln.ptr[pos] >= '0' && ln.ptr[pos] <= '9') { ++pos; continue; }
        if (ln.ptr[pos] != '.') break;
        if (ln.length-pos > 1 && ln.ptr[pos+1] >= '0' && ln.ptr[pos+1] <= '9') { pos += 2; continue; }
        break;
      }
      if (pos < ln.length) continue;
    }
    // wow, i found her!
    sscanf(line.ptr, "%lx-%*lx %s %*s %*s %*d", &addr, perms.ptr);
    if (strstr(perms.ptr, "x") !is null) { found = true; break; }
  }
  fclose(fp);
  //{ import core.stdc.stdio : printf; printf("%u: 0x%08x: %d\n", pid, addr, (found ? 1 : 0)); }
  return (found ? addr : 0);
}


// ////////////////////////////////////////////////////////////////////////// //
struct AttachedProc {
private:
  static struct Nfo {
    uint rc;
    uint pid;
    int memfd;
    bool fdwr;
    @disable this (this);
  }

private:
  uint nfop;

  void decRef () nothrow @nogc {
    if (nfop) {
      auto n = cast(Nfo*)nfop;
      if (--n.rc == 0) {
        import core.stdc.stdlib : free;
        if (n.memfd >= 0) {
          import core.sys.posix.unistd : close;
          close(n.memfd);
        }
        // addr is ignored under Linux, but should be 1 under FreeBSD in order to let the child process continue at what it had been interrupted
        ptrace(PtraceRequest.DETACH, n.pid, 1, 0); // == 0
        free(n);
      }
      nfop = 0;
    }
  }

public:
  this (uint apid) { attachTo(apid); }
  this (this) nothrow @trusted @nogc { pragma(inline, true); if (nfop) (cast(Nfo*)nfop).rc += 1; }
  ~this () nothrow @nogc { pragma(inline, true); if (nfop) decRef(); }

  void close () nothrow @nogc { pragma(inline, true); if (nfop) decRef(); }

  void attachTo (uint apid) {
    import core.stdc.stdlib : malloc, free;
    import core.stdc.string : memset;
    import core.sys.posix.sys.wait : waitpid, WIFSTOPPED;
    int status;
    close();
    if (apid == 0 || apid == 1) throw new Exception("attach failed");
    // attach, to the target application, which should cause a SIGSTOP
    if (ptrace(PtraceRequest.ATTACH, apid, null, null) == -1) throw new Exception("attach failed");
    // detach on failure, just in case
    scope(failure) ptrace(PtraceRequest.DETACH, apid, 1, 0); // == 0
    // wait for the SIGSTOP to take place
    if (waitpid(apid, &status, 0) == -1 || !WIFSTOPPED(status)) throw new Exception("error waiting target to stop");
    auto n = cast(Nfo*)malloc(Nfo.sizeof);
    if (n is null) throw new Exception("out of memory"); // hm...
    memset(n, 0, Nfo.sizeof);
    n.rc = 1;
    n.pid = apid;
    n.memfd = -666;
    nfop = cast(uint)n;
  }

  void opAssign() (in auto ref AttachedProc o) nothrow @nogc {
    // increase other's rc, and then decrease ours; doing in in another order is unsafe
    if (o.nfop) (cast(Nfo*)o.nfop).rc += 1;
    if (nfop) decRef();
    nfop = o.nfop;
  }

  @property bool valid () const nothrow @safe @nogc { pragma(inline, true); return (nfop != 0); }

  private bool openFD () nothrow @nogc {
    if (!nfop) return false;
    auto n = cast(Nfo*)nfop;
    uint pid = n.pid;
    if (n.memfd == -666) {
      import core.stdc.stdio : snprintf;
      import core.sys.posix.fcntl : open, O_RDONLY, O_RDWR;
      char[64] name = 0;
      snprintf(name.ptr, name.length-1, "/proc/%d/mem", pid);
      n.memfd = open(name.ptr, O_RDWR);
      if (n.memfd < 0) {
        n.fdwr = false;
        n.memfd = open(name.ptr, O_RDONLY);
        if (n.memfd < 0) n.memfd = -1;
      } else {
        n.fdwr = true;
      }
    }
    return (n.memfd >= 0);
  }

  T[] readBytes(T) (T[] buf, uint addr) {
    import core.sys.posix.unistd : pread;
    if (!nfop) throw new Exception("can't read from unintialized attach");
    if (buf.length > int.max/2) throw new Exception("too many elements in read buffer");
    if (cast(long)buf.length*T.sizeof > int.max/2) throw new Exception("too many elements in read buffer");
    if (buf.length == 0) return buf;
    auto rd = (0x1_0000_0000L-addr);
    if (rd < T.sizeof) throw new Exception("read error");
    if (rd > buf.length*T.sizeof) rd = buf.length*T.sizeof;
    auto n = cast(Nfo*)nfop;
    uint pid = n.pid;
    version(pt_use_mem) {
      if (!openFD) throw new Exception("can't open process memory");
      rd /= T.sizeof;
      auto len = pread(n.memfd, buf.ptr, cast(uint)(rd*T.sizeof), addr);
      if (len < T.sizeof) throw new Exception("read error");
      return buf[0..len/T.sizeof];
    } else {
      import core.stdc.errno;
      import core.stdc.string : memcpy;
      auto dp = cast(uint*)buf.ptr;
      uint len = 0;
      errno = 0;
      foreach (immutable _; 0..rd/4) {
        uint res = ptrace(PtraceRequest.PEEKDATA, pid, addr, null);
        if (res == -1 && errno != 0) throw new Exception("read error");
        *dp++ = res;
        addr += 4;
        len += 4;
        //if (ptrace(PtraceRequest.POKEDATA, pid, addr, res) != 0) throw new Exception("writing error");
      }
      if (rd%4) {
        uint res = ptrace(PtraceRequest.PEEKDATA, pid, addr, null);
        if (res == -1 && errno != 0) throw new Exception("read error");
        memcpy(dp, &res, cast(uint)(rd%4));
        len += rd%4;
      }
      if (len < T.sizeof) throw new Exception("read error");
      return buf[0..len/T.sizeof];
    }
  }

  void writeBytes (const(void)[] buf, uint addr) {
    import core.sys.posix.unistd : pwrite;
    if (!nfop) throw new Exception("can't write to unintialized attach");
    if (buf.length == 0) return;
    auto n = cast(Nfo*)nfop;
    uint pid = n.pid;
    if (!openFD) throw new Exception("can't open process memory");
    if (!n.fdwr) throw new Exception("can't open process memory for writing");
    long wr = 0x1_0000_0000L-addr;
    if (wr < buf.length) throw new Exception("write error");
    auto len = pwrite(n.memfd, buf.ptr, cast(uint)buf.length, addr);
    if (len != buf.length) throw new Exception("write error");
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public T[] procReadMem(T) (uint pid, T[] buf, uint addr) {
  if (pid <= 2) return null; //throw new Exception("read error");
  if (buf.length == 0) return buf[];
  /*
  iovec local, remote;
  local.iov_base = cast(void*)buf.ptr;
  local.iov_len = buf.length;
  remote.iov_base = cast(void*)addr;
  remote.iov_len = buf.length;
  auto len = process_vm_readv(pid, &local, 1, &remote, 1, 0);
  if (len < T.sizeof) return null; //throw new Exception("read error");
  return buf[0..len/T.sizeof];
  */
  import core.stdc.errno;
  auto bp = cast(ubyte*)buf.ptr;
  auto left = buf.length*T.sizeof;
  uint total;
  errno = 0;
  while (left > 0) {
    import core.stdc.string : memcpy;
    auto ov = ptrace(PtraceRequest.PEEKDATA, pid, addr, null);
    if (ov == -1 && errno != 0) {
      //{ import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "reading failed at 0x%08x (%u bytes left)\n", addr, cast(uint)left); }
      return null;
    }
    if (left >= 4) memcpy(bp, &ov, 4); else memcpy(bp, &ov, left);
    //{ import core.stdc.stdio : printf; printf("writing 0x%08x at 0x%08x (%u bytes left)\n", v, addr, cast(uint)left); }
    if (left <= 4) { total += left; left = 0; break; }
    left -= 4;
    addr += 4;
    bp += 4;
    total += 4;
  }
  //{ import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "procReadMem complete (%u bytes)\n", total); }
  return buf[0..total/T.sizeof];
}


public bool procWriteMem (uint pid, const(void)[] buf, uint addr) {
  if (pid <= 2) return false; //throw new Exception("write error");
  if (buf.length == 0) return true;
  /*
  iovec local, remote;
  local.iov_base = cast(void*)buf.ptr;
  local.iov_len = buf.length;
  remote.iov_base = cast(void*)addr;
  remote.iov_len = buf.length;
  auto len = process_vm_writev(pid, &local, 1, &remote, 1, 0);
  if (len != buf.length) return false; //throw new Exception("write error");
  return true;
  */
  auto bp = cast(const(ubyte)*)buf.ptr;
  auto left = buf.length;
  while (left > 0) {
    import core.stdc.string : memcpy;
    uint v = 0;
    if (left < 4) {
      import core.stdc.errno;
      errno = 0;
      auto ov = ptrace(PtraceRequest.PEEKDATA, pid, addr, null);
      if (ov == -1 && errno != 0) return false;
      v = ov;
    }
    memcpy(&v, bp, 4);
    //{ import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "writing 0x%08x at 0x%08x (%u bytes left)\n", v, addr, cast(uint)left); }
    if (ptrace(PtraceRequest.POKEDATA, pid, addr, v) != 0) return false;
    if (left <= 4) break;
    left -= 4;
    addr += 4;
    bp += 4;
  }
  //{ import core.stdc.stdio : stderr, fprintf; fprintf("procWriteMem complete\n"); }
  return true;
}


// ////////////////////////////////////////////////////////////////////////// //
// code injector
// some code was taken from "linux-inject" project: https://github.com/gaffe23/linux-inject
private:
import iv.olly.asm1;
import iv.olly.disasm2;


extern(C) nothrow @nogc {
  enum RTLD_LAZY = 0x00001; // POSIX
  void* __libc_dlopen_mode (const(char)* name, int mode);
  void* __libc_dlsym (void* soh, const(char)* name);
  int __libc_dlclose (void* soh);
}


private uint getFunctionAddress (const(char)[] funcname) {
  //import core.sys.linux.dlfcn;
  if (funcname.length == 0 || funcname.length > 127) return false;
  char[129] buf = 0;
  buf[0..funcname.length] = funcname[];
  void* self = __libc_dlopen_mode("libc.so.6", RTLD_LAZY);
  void* funcAddr = __libc_dlsym(self, buf.ptr);
  return cast(uint)funcAddr;
}


// ////////////////////////////////////////////////////////////////////////// //
public bool injectSO (int pid, const(char)[] libname, const(char)[] initarg) {
  static immutable string injectCode = q{
    ; call __libc_dlopen_mode() to load the shared library
    push 1           ; 2nd argument to __libc_dlopen_mode(): flag = RTLD_LAZY
    push libnameofs  ; 1st argument to __libc_dlopen_mode(): filename = the buffer we allocated earlier
    call dlopen      ; call __libc_dlopen_mode()
    add esp,4*2      ; as it is cdecl, we need to pop arguments
    ; exit if library is not loaded
    or  eax,eax
    jz  quit

    ; call __libc_dlsym() to find the symbol
    push initnameofs ; 2nd argument to __libc_dlsym() -- function name
    push eax         ; 1st argument to __libc_dlsym() -- library handle
    call dlsym       ; call __libc_dlsym()
    add esp,4*2      ; as it is cdecl, we need to pop arguments

    ; call the init function if it is there
    or eax,eax
    jz skipcall

    push iniargofs   ; argument
    call eax         ; call init function
    pop edx          ; as it is cdecl, we need to pop arguments (this is smaller than fixing esp)
    jmp quit

  skipcall:
    mov eax,1        ; success flag

  quit:
    int3         ; final out
  };

  static bool attachTo (uint apid) {
    import core.stdc.stdlib : malloc, free;
    import core.stdc.string : memset;
    import core.sys.posix.sys.wait : waitpid, WIFSTOPPED;
    int status;
    // attach, to the target application, which should cause a SIGSTOP
    if (ptrace(PtraceRequest.ATTACH, apid, null, null) == -1) return false;
    // detach on failure, just in case
    // wait for the SIGSTOP to take place
    if (waitpid(apid, &status, 0) == -1 || !WIFSTOPPED(status)) {
      ptrace(PtraceRequest.DETACH, apid, 1, 0);
      return false;
    }
    return true;
  }

  static bool singleStep (uint apid) {
    import core.stdc.stdlib : malloc, free;
    import core.stdc.string : memset;
    import core.sys.posix.sys.wait : waitpid, WIFEXITED;
    int status;
    if (ptrace(PtraceRequest.SINGLESTEP, apid, null, null) == -1) return false;
    if (waitpid(apid, &status, 0) == -1 || WIFEXITED(status)) return false;
    return true;
  }

  static bool singleStepUntilInt3 (uint apid, uint saddr, uint len, user_regs_struct* regsp) {
    import core.stdc.stdlib : malloc, free;
    import core.stdc.string : memset;
    import core.sys.posix.sys.wait : waitpid, WIFEXITED;
    user_regs_struct regs;
    if (regsp is null) regsp = &regs;
    uint oeip = 0;
    for (;;) {
      int status;
      if (ptrace(PtraceRequest.SINGLESTEP, apid, null, null) == -1) return false;
      if (waitpid(apid, &status, 0) == -1 || WIFEXITED(status)) return false;
      if (ptrace(PtraceRequest.GETREGS, apid, null, regsp) == -1) return false;
      ubyte[MAXCMDSIZE] ib;
      procReadMem(apid, ib[], regsp.eip);
      {
        import core.stdc.stdio : stderr, fprintf;
        //fprintf(stderr, "EIP=0x%08x (%02X)\n", regsp.eip, ib);
        DisasmData da;
        DAConfig cfg;
        cfg.tabarguments = true;
        auto dclen = disasm(ib[], regsp.eip, &da, DA_DUMP|DA_TEXT|DA_HILITE, &cfg);
        if (dclen == 0) {
          fprintf(stderr, "EIP=0x%08x FUCK!\n", regsp.eip);
        } else {
          import std.stdio : stderr;
          //stderr.writefln("0x%08x: %-16s %s", da.ip, da.dumpstr, da.resstr);
        }
        if (oeip == regsp.eip) {
          if (ib[0] == 0xf4) {
            import std.stdio : stderr;
            stderr.writefln("0x%08x: %-16s %s", da.ip, da.dumpstr, da.resstr);
            return false;
          }
        }
        oeip = regsp.eip;
      }
      if (ib[0] == 0xcc) break;
    }
    return true;
  }

  static bool int3Step (string msg, uint apid, uint saddr, uint len, user_regs_struct* regsp) {
    version(all) {
      import core.stdc.stdlib : malloc, free;
      import core.stdc.string : memset;
      import core.sys.posix.signal : siginfo_t, SIGTRAP;
      import core.sys.posix.sys.wait : waitpid, WIFEXITED;
      user_regs_struct regsxx;
      if (regsp is null) regsp = &regsxx;
      //{ import std.stdio : stderr; stderr.writeln("STEPPING: ", msg); }
      for (;;) {
        int status;
        if (ptrace(PtraceRequest.CONT, apid, null, null) == -1) return false;
        if (waitpid(apid, &status, 0) == -1 || WIFEXITED(status)) {
          { import core.stdc.stdio : printf; printf("XFUCK0\n"); }
          return false;
        }
        // check what signal we received
        siginfo_t tsig;
        if (ptrace(PtraceRequest.GETSIGINFO, apid, null, &tsig) == -1) {
          { import core.stdc.stdio : printf; printf("XFUCK1\n"); }
          return false;
        }
        if (tsig.si_signo == SIGTRAP) {
          memset(regsp, 0, user_regs_struct.sizeof);
          if (ptrace(PtraceRequest.GETREGS, apid, null, regsp) == -1) {
            { import core.stdc.stdio : printf; printf("XFUCK2\n"); }
            return false;
          }
          if (regsp.eip >= saddr && regsp.eip <= saddr+len) {
            ubyte bt;
            if (procReadMem(apid, (&bt)[0..1], regsp.eip).length == 1 && bt == 0x90) {
              import core.stdc.stdio : printf;
              printf("eip=0x%08x eax=0x%08x ebx=0x%08x ecx=0x%08x edx=0x%08x esi=0x%08x edi=0x%08x\n", regsp.eip, regsp.eax, regsp.ebx, regsp.ecx, regsp.edx, regsp.esi, regsp.edi);
              continue;
            }
            //{ import core.stdc.stdio : printf; printf("*** %02X 0x%08x\n", bt, regsp.eip); }
            //{ import core.stdc.stdio : printf; printf("EIP=0x%08x\n", regsp.eip); }
            return true;
          }
          { import core.stdc.stdio : printf; printf("trap signal at 0x%08x\n", regsp.eip); }
          // not in our function, continue
        } else {
          // some other signal, skip it
          { import core.stdc.stdio : printf; printf("non-trap signal at 0x%08x (%u)\n", regsp.eip, cast(uint)tsig.si_signo); }
          return false;
        }
      }
      version(all) {
        import core.thread;
        import core.time;
        Thread.sleep(500.msecs);
      }
      return true;
    } else {
      { import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "========================\n"); }
      user_regs_struct regs;
      if (regsp is null) regsp = &regs;
      if (ptrace(PtraceRequest.GETREGS, apid, null, regsp) == -1) return false;
      { import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "ecx=0x%08x; edi=0x%08x\n", regsp.ecx, regsp.edi); }
      auto res = singleStepUntilInt3(apid, saddr, len, regsp);
      if (res) {
        // skip int3
        //++regsp.eip;
        //if (ptrace(PtraceRequest.SETREGS, apid, null, regsp) == -1) return false;
      }
      { import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "res:%u; eax=0x%08x\n", (res ? 1 : 0), regsp.eax); }
      return res;
    }
  }

  import core.sys.posix.unistd : getpid;

  if (pid <= 1 || libname.length == 0 || libname.length > 1023) return false;
  if (initarg.length > 1023) return false; // alas

  auto mypid = getpid();
  auto mylibcaddr = findLibInPrey(mypid, "libc-");
  if (mylibcaddr == 0) {
    { import core.stdc.stdio : printf; printf("no libc found in our process (WTF?!)\n"); }
    return false;
  }

  // find a good address to copy code to
  uint ijAddr = findXSpaceInPrey(pid);
  if (ijAddr == 0) {
    { import core.stdc.stdio : printf; printf("can't find free space to paste our code\n"); }
    return false;
  }
  //{ import core.stdc.stdio : printf; printf("[0x%08x]\n", addr); }

  // find the addresses of the syscalls that we'd like to use inside the
  // target, as loaded inside THIS process (i.e. NOT the target process)
  auto mallocAddr = getFunctionAddress("malloc");
  if (mallocAddr == 0) {
    { import core.stdc.stdio : printf; printf("no malloc found in our process\n"); }
    return false;
  }
  auto freeAddr = getFunctionAddress("free");
  if (freeAddr == 0) {
    { import core.stdc.stdio : printf; printf("no free found in our process\n"); }
    return false;
  }
  auto dlopenAddr = getFunctionAddress("__libc_dlopen_mode");
  if (dlopenAddr == 0) {
    { import core.stdc.stdio : printf; printf("no dlopen found in our process\n"); }
    return false;
  }
  auto dlsymAddr = getFunctionAddress("__libc_dlsym");
  if (dlopenAddr == 0) {
    { import core.stdc.stdio : printf; printf("no dlsym found in our process\n"); }
    return false;
  }

  // use the base address of libc to calculate offsets for the syscalls we want to use
  uint mallocOffset = mallocAddr-mylibcaddr;
  uint freeOffset = freeAddr-mylibcaddr;
  uint dlopenOffset = dlopenAddr-mylibcaddr;
  uint dlsymOffset = dlsymAddr-mylibcaddr;

  // get the target process' libc address and use it to find the
  // addresses of the syscalls we want to use inside the target process
  uint targetLibcAddr = findLibInPrey(pid, "libc-");
  if (targetLibcAddr == 0) return false;


  ubyte[] ijcode;
  uint ijcodeUsed = 0;
  // build injection code
  {
    auto ass = new Assembler(ijAddr);
    ass.addLabel("malloc", targetLibcAddr+mallocOffset);
    ass.addLabel("free", targetLibcAddr+freeOffset);
    ass.addLabel("dlopen", targetLibcAddr+dlopenOffset);
    ass.addLabel("dlsym", targetLibcAddr+dlsymOffset);
    ass.addLines(injectCode);

    void putAsmStr (const(void)[] s) {
      import std.format : format;
      foreach (ubyte b; cast(const(ubyte[]))s) {
        string ln = " db 0x%02x".format(b);
        //{ import iv.vfs.io; writeln("[", ln, "]"); }
        ass.addLines(ln);
      }
      ass.addLines(" db 0x00");
    }

    // put strings
    ass.addLabelHere("libnameofs");
    putAsmStr(libname);
    ass.addLabelHere("initnameofs");
    putAsmStr("initme");
    ass.addLabelHere("iniargofs");
    putAsmStr(initarg);

    ijcode = ass.getCode();
    //{ import std.stdio; writeln("assembled to ", ijcode.length, " bytes"); }
    //ass.disasmCode(ijcode, ass.orgpc);
    ijcodeUsed = cast(uint)ijcode.length;
  }
  scope(exit) ijcode.destroy;

  user_regs_struct oldregs, regs;

  if (!attachTo(pid)) {
    { import core.stdc.stdio : printf; printf("can't attach to target process\n"); }
    return false;
  }

  if (ptrace(PtraceRequest.GETREGS, pid, null, &oldregs) == -1) {
    ptrace(PtraceRequest.DETACH, pid, 1, 0);
    { import core.stdc.stdio : printf; printf("can't get registers of target process\n"); }
    return false;
  }

  ubyte[] bkdata = new ubyte[](ijcode.length);
  bool bkdataUsed = false;

  scope(exit) {
    // restore data and registers, and detach
    if (bkdataUsed) procWriteMem(pid, bkdata[], ijAddr);
    bkdata.destroy;
    ptrace(PtraceRequest.SETREGS, pid, null, &oldregs);
    ptrace(PtraceRequest.DETACH, pid, 1, 0);
    //{ import core.stdc.stdio : printf; printf("prey process restored\n"); }
  }

  // if prey is somewhere below libc, trace it until it returns
  // this should prevent deadlocks on multithreaded apps (i hope)
  while (oldregs.eip >= targetLibcAddr) {
    //{ import core.stdc.stdio : printf; printf("EIP=0x%08x\n", oldregs.eip); }
    if (!singleStep(pid)) {
      { import core.stdc.stdio : printf; printf("singlestepping failed\n"); }
      return false;
    }
    if (ptrace(PtraceRequest.GETREGS, pid, null, &oldregs) == -1) {
      { import core.stdc.stdio : printf; printf("can't get registers of target process\n"); }
      return false;
    }
  }
  regs = oldregs;
  //{ import core.stdc.stdio : printf; printf("EIP=0x%08x\n", regs.eip); }

  // back up whatever data used to be at the address we want to modify.
  if (procReadMem(pid, bkdata[], ijAddr).length != bkdata.length) {
    { import core.stdc.stdio : printf; printf("can't read process memory\n"); }
    return false;
  }
  bkdataUsed = true;

  if (!procWriteMem(pid, ijcode[], ijAddr)) {
    { import core.stdc.stdio : printf; printf("can't write process memory\n"); }
    return false;
  }

  // execute our injected code
  regs.eip = ijAddr;
  if (ptrace(PtraceRequest.SETREGS, pid, null, &regs) == -1) {
    { import core.stdc.stdio : printf; printf("can't set registers of target process\n"); }
    return false;
  }
  if (!int3Step("boo", pid, ijAddr, ijcodeUsed, &regs)) {
    { import core.stdc.stdio : printf; printf("int3 stepping failed\n"); }
    return false;
  }
  //{ import core.stdc.stdio : printf; printf("EIP=0x%08x\n", regs.eip); }

  // at this point, the target should has success in eax (!=0)
  uint targetBuf = regs.eax;
  if (regs.eax == 0) {
    { import core.stdc.stdio : printf; printf("target process failed to initialize\n"); }
    return false;
  }

  return true;
}


// ////////////////////////////////////////////////////////////////////////// //
private:
import core.sys.posix.sys.uio : iovec;
import core.sys.posix.sys.types : pid_t, ssize_t;
import core.stdc.config : c_ulong;
extern(C) nothrow @nogc:

ssize_t process_vm_readv (pid_t pid, const(iovec)* local_iov, c_ulong liovcnt, const(iovec)* remote_iov, c_ulong riovcnt, c_ulong flags);
ssize_t process_vm_writev (pid_t pid, const(iovec)* local_iov, c_ulong liovcnt, const(iovec)* remote_iov, c_ulong riovcnt, c_ulong flags);


// Type of the REQUEST argument to `ptrace()`
enum PtraceRequest : int {
  TRACEME = 0, // indicate that the process making this request should be traced; all signals received by this process can be intercepted by its parent, and its parent can use the other `ptrace' requests
  PEEKTEXT = 1, // return the word in the process's text space at address ADDR
  PEEKDATA = 2, // return the word in the process's data space at address ADDR
  PEEKUSER = 3, // return the word in the process's user area at offset ADDR
  POKETEXT = 4, // write the word DATA into the process's text space at address ADDR
  POKEDATA = 5, // write the word DATA into the process's data space at address ADDR
  POKEUSER = 6, // write the word DATA into the process's user area at offset ADDR
  CONT = 7, // continue the process
  KILL = 8, // kill the process
  SINGLESTEP = 9, // single step the process. This is not supported on all machines
  GETREGS = 12, // get all general purpose registers used by a processes. This is not supported on all machines
  SETREGS = 13, // set all general purpose registers used by a processes. This is not supported on all machines
  GETFPREGS = 14, // get all floating point registers used by a processes. This is not supported on all machines
  SETFPREGS = 15, // set all floating point registers used by a processes. This is not supported on all machines
  ATTACH = 16, // attach to a process that is already running
  DETACH = 17, // detach from a process attached to with ATTACH
  GETFPXREGS = 18, // get all extended floating point registers used by a processes. This is not supported on all machines
  SETFPXREGS = 19, // set all extended floating point registers used by a processes. This is not supported on all machines
  SYSCALL = 24, // continue and stop at the next (return from) syscall
  SETOPTIONS = 0x4200, // set ptrace filter options
  GETEVENTMSG = 0x4201, // get last ptrace message
  GETSIGINFO = 0x4202, // get siginfo for process
  SETSIGINFO = 0x4203, // set new siginfo for process
  GETREGSET = 0x4204, // get register content
  SETREGSET = 0x4205, // set register content
  SEIZE = 0x4206, // like ATTACH, but do not force tracee to trap and do not affect signal or group stop state
  INTERRUPT = 0x4207, // trap seized tracee
  LISTEN = 0x4208, // wait for next group event
  PEEKSIGINFO = 0x4209,
  GETSIGMASK = 0x420a,
  SETSIGMASK = 0x420b,
  SECCOMP_GET_FILTER = 0x420c,
}


// flag for LISTEN
enum uint PTRACE_SEIZE_DEVEL = 0x80000000u;

// options set using SETOPTIONS
alias PtraceSetOptions = uint;
enum /*PtraceSetOptions*/ : uint {
  PTRACE_O_TRACESYSGOOD    = 0x00000001u,
  PTRACE_O_TRACEFORK       = 0x00000002u,
  PTRACE_O_TRACEVFORK      = 0x00000004u,
  PTRACE_O_TRACECLONE      = 0x00000008u,
  PTRACE_O_TRACEEXEC       = 0x00000010u,
  PTRACE_O_TRACEVFORKDONE  = 0x00000020u,
  PTRACE_O_TRACEEXIT       = 0x00000040u,
  PTRACE_O_TRACESECCOMP    = 0x00000080u,
  PTRACE_O_EXITKILL        = 0x00100000u,
  PTRACE_O_SUSPEND_SECCOMP = 0x00200000u,
  PTRACE_O_MASK            = 0x003000ffu,
}

// wait extended result codes for the above trace options
enum PtraceEventCodes {
  FORK       = 1,
  VFORK      = 2,
  CLONE      = 3,
  EXEC       = 4,
  VFORK_DONE = 5,
  EXIT       = 6,
  SECCOMP    = 7,
}

// arguments for PEEKSIGINFO
struct PtracePeekSigInfoArgs {
  ulong off;  // From which siginfo to start
  uint flags; // Flags for peeksiginfo
  int nr;     // How many siginfos to take
}

enum uint PTRACE_PEEKSIGINFO_SHARED = 1u<<0; // read signals from a shared (process wide) queue

/* Perform process tracing functions. REQUEST is one of the values above, and determines the action to be taken.
 * For all requests except TRACEME, PID specifies the process to be traced.
 *
 *  PID and the other arguments described above for the various requests should
 *  appear (those that are used for the particular request) as:
 *    pid_t PID, void *ADDR, int DATA, void *ADDR2
 *  after REQUEST. */
int ptrace (PtraceRequest request, ...);


/* These are the 32-bit x86 structures.  */
struct user_fpregs_struct {
  uint cwd;
  uint swd;
  uint twd;
  uint fip;
  uint fcs;
  uint foo;
  uint fos;
  uint[20] st_space;
}

struct user_fpxregs_struct {
  ushort cwd;
  ushort swd;
  ushort twd;
  ushort fop;
  uint fip;
  uint fcs;
  uint foo;
  uint fos;
  uint mxcsr;
  uint reserved;
  uint[32] st_space;  // 8*16 bytes for each FP-reg = 128 bytes
  uint[32] xmm_space; // 8*16 bytes for each XMM-reg = 128 bytes
  uint[56] padding;
}

struct user_regs_struct {
  uint ebx;
  uint ecx;
  uint edx;
  uint esi;
  uint edi;
  uint ebp;
  uint eax;
  uint xds;
  uint xes;
  uint xfs;
  uint xgs;
  uint orig_eax;
  uint eip;
  uint xcs;
  uint eflags;
  uint esp;
  uint xss;
}

struct user {
  user_regs_struct regs;
  int u_fpvalid;
  user_fpregs_struct i387;
  uint u_tsize;
  uint u_dsize;
  uint u_ssize;
  uint start_code;
  uint start_stack;
  uint signal;
  int reserved;
  user_regs_struct* u_ar0;
  user_fpregs_struct* u_fpstate;
  uint magic;
  char[32] u_comm;
  int[8] u_debugreg;
}
