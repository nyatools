/* Written by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module nya is aliced;

import iv.autocomplete : acbuild = autocomplete;
import iv.editline;
import iv.strex;
import iv.vfs;
import iv.vfs.io;
import iv.ncrpc;

import addrstore;
import procutil;
//import sockchan;
import ncproto;

import ncproto;


// ////////////////////////////////////////////////////////////////////////// //
string getSOName () {
  version(rdmd) {
    return "/home/ketmar/back/D/prj/nyatools/mono/injed.so";
  } else {
    import std.file : thisExePath;
    import std.path : buildPath, dirName;
    return buildPath(thisExePath.dirName, "mono", "injed.so");
  }
}


// ////////////////////////////////////////////////////////////////////////// //
alias string[] delegate (const(char)[] pfx) BuildACListCB;

__gshared BuildACListCB[string] knownACB;


// ////////////////////////////////////////////////////////////////////////// //
string[] acAtFieldsCB (const(char)[] pfx) {
  import std.algorithm : startsWith;
  if (monoClasses.length == 0) return null; // no wai
  //writeln("fields; pfx=[", pfx, "]");
  string[] lst;
  if (pfx.length == 0) {
    foreach (const ref cls; monoClasses) lst ~= cls.fullname;
  } else {
    // class names
    foreach (const ref cls; monoClasses) if (cls.name.startsWith(pfx)) lst ~= cls.name;
    // namespaces
    foreach (const ref cls; monoClasses) if (cls.fullnamenoasm.startsWith(pfx)) lst ~= cls.fullnamenoasm;
    // assemblies
    foreach (const ref cls; monoClasses) if (cls.fullname != cls.fullnamenoasm && cls.fullname.startsWith(pfx)) lst ~= cls.fullname;
  }
  // done
  return lst;
}


// ////////////////////////////////////////////////////////////////////////// //
shared static this () {
  import std.functional : toDelegate;
  knownACB["@fields"] = toDelegate(&acAtFieldsCB);
}


// ////////////////////////////////////////////////////////////////////////// //
class EditLineAC : EditLine {
  this () { super(); }

  override void autocomplete () {
    if (!line.canAutocomplete) { beep; return; }
    int wc = line.wordCount;
    if (wc > 2) return;
    auto acwp = line.acWordPos;
    auto acnum = line.acWordNum;
    string[] lst;
    if (acnum == 0) {
      // we want autocomplete command
      lst = acbuild(line.word(0).idup,
        // mono dissector
        "@asmname",
        "@asms",
        "@classes",
        "@fields",
        "@writeclasses",
        //
        "asbyte",
        "asdouble",
        "asfloat",
        "asint",
        "aslong",
        "asshort",
        "killall",
        "list",
        "name",
        "new",
        "only",
        "quit",
        "set",
      );
    } else if (acnum == 1) {
      // autocomplete argument
      if (auto cb = line.word(0) in knownACB) {
        //clearOutput(); wrt("FOUND ACCB! ["); wrt(line[acwp]); wrt("]\n");
        lst = (*cb)(line[acwp]);
        if (lst.length == 0) { beep; return; }
        lst = acbuild(line[acwp].idup, lst);
      } else {
        beep;
        return;
      }
    } else {
      // unknown (TODO)
      beep;
      return;
    }
    if (lst.length == 0) { beep; return; }
    if (lst.length == 1) {
      if (!line.acWordReplaceSpaced(lst[0])) { beep; return; }
    } else if (lst.length > 1) {
      if (!line.acWordReplace(lst[0])) { beep; return; }
      clearOutput();
      auto len = (lst.length > 24 ? 24 : lst.length);
      wrt("====================\n");
      foreach (auto s; lst[1..len]) { wrt(s); wrt("\r\n"); }
      if (len < lst.length) { wrt("..."); wrtuint(cast(int)lst.length-len); wrt(" more...\n"); }
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared EditLineAC lineedit;


// ////////////////////////////////////////////////////////////////////////// //
void cleanupMemory () {
  { import core.memory : GC; GC.collect; GC.minimize; }
}


// ////////////////////////////////////////////////////////////////////////// //
EditLine.Result readLine (uint vidx) {
  import std.format : format;
  if (lineedit is null) lineedit = new EditLineAC();
  string tn = taskNames[vidx];
  if (tn.length) {
    lineedit.prompt = "(%s)[%s] %s>".format(vidx+1, foundAddrs[vidx].count, tn);
  } else {
    lineedit.prompt = "(%s)[%s]>".format(vidx+1, foundAddrs[vidx].count);
  }
  auto res = lineedit.readline();
  cleanupMemory();
  return res;
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared AssemblyInfo[] monoAsms;
__gshared NCClassInfo[] monoClasses;


void monoClear () {
  monoAsms = null;
  monoClasses = null;
}


// return index or -1
int findClassByName (const(char)[] name) {
  // by id or index
  try {
    uint cid = name.xto!uint;
    foreach (immutable idx, const ref cls; monoClasses) if (cls.id == cid) return cast(int)idx;
    if (cid < monoClasses.length) return cid;
  } catch (Exception) {}
  // by name
  foreach (immutable idx, const ref cls; monoClasses) {
    if (cls.fullname == name) return cast(int)idx; // exact match
  }
  int res = -1;
  foreach (immutable idx, const ref cls; monoClasses) {
    if (cls.isNameEqu(name)) {
      if (res != -1) return -1;
      res = cast(int)idx;
    }
  }
  return res;
}


void monoScan () {
  {
    monoAsms = injSock.rpcall!(enumAssemblies, "mono.")();
    writeln(monoAsms.length, " mono assemblies found");
  }
  {
    monoClasses = injSock.rpcall!(getClassList, "mono.")();
    writeln(injSock.bytesReceived, " bytes received");
    foreach (ref cls; monoClasses) {
      foreach (const ref ass; monoAsms) {
        if (cls.asmid == ass.id) {
          cls.asmname = ass.name;
          break;
        }
      }
      // build full name
      cls.fullname = cls.asmname;
      if (cls.nspace.length) { if (cls.fullname.length) cls.fullname ~= '.'; cls.fullname ~= cls.nspace; }
      if (cls.name.length) { if (cls.fullname.length) cls.fullname ~= '.'; cls.fullname ~= cls.name; }
      // build full name w/o assembly
      cls.fullnamenoasm = cls.nspace;
      if (cls.name.length) { if (cls.fullnamenoasm.length) cls.fullnamenoasm ~= '.'; cls.fullnamenoasm ~= cls.name; }
    }
    writeln(monoClasses.length, " mono classes found");
  }
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared uint preypid;
__gshared MemoryRegionInfo[] memregs;
__gshared string nyaSockName;
__gshared UDSocket nyaSock, injSock;
__gshared bool codeInjected = false;

__gshared InjCodeInfo injInfo;


void restart (uint newpid) {
  import core.sys.posix.unistd : getpid;
  import std.string : format;
  __gshared uint counter = 0;
  injSock.close();
  nyaSock.close();
  monoClear();
  if (newpid < 1) throw new Exception("invalid pid");
  preypid = newpid;
  reloadRegions();
  nyaSockName = "/nya-tools/injected/%s_%s_%s".format(getpid(), newpid, counter++);
  nyaSock.create(nyaSockName);
  codeInjected = false;
}


// ////////////////////////////////////////////////////////////////////////// //
enum MaxVals = 10;
__gshared uint nums;
__gshared AddrStore[MaxVals] foundAddrs;
__gshared string[MaxVals] taskNames;


// ////////////////////////////////////////////////////////////////////////// //
// terrible hack!
const(char)[] commaed (ulong l) {
  __gshared uint bufnum;
  __gshared char[256][32] bufs;
  auto res = bufs.ptr[bufnum++][];
  bufnum %= bufs.length;
  uint pos = cast(uint)res.length;
  ubyte digsleft = 3;
  do {
    if (digsleft == 0) { res.ptr[--pos] = ','; digsleft = 3; }
    res.ptr[--pos] = cast(char)(l%10+'0');
    l /= 10;
    --digsleft;
  } while (l != 0);
  return res[pos..$];
}


// ////////////////////////////////////////////////////////////////////////// //
void injectCode () {
  import core.sys.posix.unistd : getpid;
  import std.file : exists;
  import std.format : format;
  __gshared uint counter = 0;
  if (codeInjected) return;
  // copy so
  auto soname = getSOName();
  if (!soname.exists) {
    stderr.writeln("ERROR: '", soname, "' not found!");
    return;
  }
  string newSOName = "/tmp/_injed_%s_%s_%s.so\0".format(getpid(), preypid, counter++);
  scope(exit) {
    import core.sys.posix.unistd : unlink;
    unlink(newSOName.ptr);
  }
  try {
    auto fi = VFile(soname, "r");
    auto fo = VFile(newSOName[0..$-1], "w");
    char[1024] buf = void;
    while (fi.tell < fi.size) {
      auto rd = fi.size-fi.tell;
      if (rd > buf.length) rd = buf.length;
      fi.rawReadExact(buf[0..cast(uint)rd]);
      fo.rawWrite(buf[0..cast(uint)rd]);
    }
    fi.close();
    fo.close();
    {
      import core.sys.posix.sys.stat : chmod;
      chmod(newSOName.ptr, 0o755);
    }
    //writeln("'", soname, "' -> '", newSOName[0..$-1], "'");
  } catch (Exception) {
    stderr.writeln("ERROR: .so copying error!");
    return;
  }
  nyaSock.listen();
  if (!injectSO(preypid, newSOName[0..$-1], nyaSockName)) {
    writeln("injecting failed");
  } else {
    writeln("injecting succeded, waiting for pingback");
    injSock = nyaSock.accept();
    writeln("pingback received, injection complete (fd=", injSock.fd, ")");
    injSock.ncReceivePingback(injInfo);
    writefln("pray has %sWINE, pray has %smono", (injInfo.hasWine ? "" : "no "), (injInfo.hasMono ? "" : "no "));
    codeInjected = true;
    if (injInfo.hasMono) monoScan();
  }
}


// ////////////////////////////////////////////////////////////////////////// //
void reloadRegions () {
  import std.algorithm : max, min;
  memregs = readMemoryRegions(preypid, true);
  uint total = 0, minsz = uint.max, maxsz = 0;
  foreach (const ref reg; memregs) {
    total += reg.end-reg.start;
    minsz = min(minsz, reg.end-reg.start);
    maxsz = max(maxsz, reg.end-reg.start);
  }
  uint mbs = (total/1024)*10/1024;
  writefln("%s.%s megabytes, %s regons (min:%s; max:%s)", mbs/10, mbs%10, memregs.length, commaed(minsz), commaed(maxsz));
  cleanupMemory();
}


enum IsValidScanType(T) = (is(T == float) || is(T == double) || (__traits(isIntegral, T) && T.sizeof <= 8));

template ScanType2VT(T) {
       static if (is(T == float)) enum ScanType2VT = VType.Float;
  else static if (is(T == double)) enum ScanType2VT = VType.Double;
  else static if (is(T == byte) || is(T == ubyte)) enum ScanType2VT = VType.Byte;
  else static if (is(T == short) || is(T == ushort)) enum ScanType2VT = VType.Word;
  else static if (is(T == int) || is(T == uint)) enum ScanType2VT = VType.Int;
  else static if (is(T == long) || is(T == ulong)) enum ScanType2VT = VType.Long;
  else static assert(0, "invalid scan type");
}


// this can't be used in multiple threads anyway
void scanRegion(T) (AttachedProc atp, T val, uint start, uint end, scope void delegate (uint addr, ubyte vt) savea) if (IsValidScanType!T) {
  enum BufSize = 4*1024*1024;
  import core.stdc.stdlib : malloc, free;
  __gshared ubyte* buf;
  if (buf is null) {
    buf = cast(ubyte*)malloc(BufSize+8);
    if (buf is null) assert(0, "out of memory");
  }
  ubyte vt = ScanType2VT!T;
  while (start < end && end-start >= T.sizeof) {
    uint len = end-start;
    uint next = start;
    if (len > BufSize) {
      len = BufSize;
      next += BufSize-T.sizeof;
    } else {
      next += len;
    }
    auto rd = atp.readBytes(buf[0..len], start);
    if (rd.length != len) assert(0, "wtf?!");
    auto dp = buf;
    while (len >= T.sizeof) {
      import core.stdc.string : memchr, memcmp;
      auto np = cast(ubyte*)memchr(dp, *cast(ubyte*)&val, len);
      if (np is null) break;
      uint ofs = cast(uint)(np-dp);
      len -= ofs;
      start += ofs;
      dp += ofs;
      if (memcmp(dp, &val, T.sizeof) == 0) savea(start, vt);
      --len;
      ++start;
      ++dp;
    }
    start = next;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
void doNewScanBytes(T) (uint vidx, T val) if (IsValidScanType!T) {
  foundAddrs[vidx].clear;
  reloadRegions();
  auto atp = AttachedProc(preypid);
  foreach (const ref reg; memregs) {
    scanRegion(atp, val, reg.start, reg.end, (uint addr, ubyte vt) { foundAddrs.ptr[vidx].addAddress(addr, vt); });
  }
  writeln(foundAddrs[vidx].count, " matches found");
  cleanupMemory();
}


void doFilter(T) (uint vidx, T val) if (IsValidScanType!T) {
  auto atp = AttachedProc(preypid);
  auto reg = ASRangeD(foundAddrs[vidx]); // this clears fa
  ubyte vt = ScanType2VT!T;
  T mv = void;
  while (!reg.empty) {
    auto va = reg.front;
    reg.popFront;
    if ((va.vt&vt) != 0) {
      if (atp.readBytes((&mv)[0..1], va.addr).length == 1) {
        if (mv == val) foundAddrs.ptr[vidx].addAddress(va.addr, vt);
      }
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
void listMatches (uint vidx) {
  if (foundAddrs[vidx].count == 0) return;
  auto atp = AttachedProc(preypid);
  uint idx = -1;
  foreach (auto va; ASRange(foundAddrs[vidx])) {
    ++idx;
    writef("[%3s] [%s] 0x%08x ", idx, vtName(va.vt), va.addr);
    if (va.vt&VType.IMask) {
      ulong v = 0;
      if (atp.readBytes((cast(ubyte*)&v)[0..maxVTSize(va.vt)], va.addr).length == maxVTSize(va.vt)) writeln(v); else writeln("ERROR");
    } else if (va.vt == VType.Float) {
      float v = 0;
      if (atp.readBytes((&v)[0..1], va.addr).length == 1) writeln(v); else writeln("ERROR");
    } else if (va.vt == VType.Double) {
      double v = 0;
      if (atp.readBytes((&v)[0..1], va.addr).length == 1) writeln(v); else writeln("ERROR");
    } else {
      writeln("ERROR");
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
void xfilter(T) (uint vidx, T val) if (IsValidScanType!T) {
  auto olen = foundAddrs[vidx].count;
  for (;;) {
    auto xo = foundAddrs[vidx].count;
    doFilter(vidx, val);
    if (foundAddrs[vidx].count == 0 || foundAddrs[vidx].count == xo) break;
  }
  writeln("reduced from ", olen, " to ", foundAddrs[vidx].count, " matches");
}


// ////////////////////////////////////////////////////////////////////////// //
void setVal(T) (AttachedProc atp, uint addr, T val) if (IsValidScanType!T) {
  atp.writeBytes((&val)[0..1], addr);
}


// ////////////////////////////////////////////////////////////////////////// //
T xto(T) (const(char)[] s) if (__traits(isIntegral, T) && T.sizeof <= 8) {
  import std.conv : to;
  bool neg = false;
       if (s.length > 0 && s[0] == '-') { neg = true; s = s[1..$]; }
  else if (s.length > 0 && s[0] == '+') { s = s[1..$]; }
  if (s.length == 0) throw new Exception("invalid value");
  ulong v;
  if (s.length > 2 && s[0] == '0' && (s[1] == 'x' || s[1] == 'X')) {
    v = to!ulong(s[2..$], 16);
  } else {
    v = to!ulong(s, 10);
  }
  if (neg) {
    long nv = v;
    if (nv < 0) throw new Exception("invalid value");
    nv = -nv;
    if (nv > 0) throw new Exception("invalid value");
         static if (T.sizeof == 1) { if (nv < byte.min || nv > byte.max) throw new Exception("invalid value"); }
    else static if (T.sizeof == 2) { if (nv < short.min || nv > short.max) throw new Exception("invalid value"); }
    else static if (T.sizeof == 4) { if (nv < int.min || nv > int.max) throw new Exception("invalid value"); }
    return cast(T)nv;
  } else {
         static if (T.sizeof == 1) { if (v > ubyte.max) throw new Exception("invalid value"); }
    else static if (T.sizeof == 2) { if (v > ushort.max) throw new Exception("invalid value"); }
    else static if (T.sizeof == 4) { if (v > uint.max) throw new Exception("invalid value"); }
    return cast(T)v;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
//import monox;
void main (string[] args) {
  //rpcRegisterModuleEndpoints!monox;

  uint pid = 0;
  if (args.length > 1) {
    import std.conv : to;
    try {
      pid = to!uint(args[1]);
    } catch (Exception e) {
      int p = findProcessByName(args[1]);
      if (p < 0) {
        writeln("process '", args[1], "' not found!");
        return;
      }
      pid = p;
      writeln("process '", args[1], "' has pid ", pid);
    }
  }
  restart(pid);

  uint vidx = 0; // current workset
  // last search values and types for all worksets
  ulong[MaxVals] lastU64 = 0;
  float[MaxVals] lastF32 = 0;
  double[MaxVals] lastF64 = 0;
  ubyte[MaxVals] lastType = VType.Int;
  bool[MaxVals] wasSearched = false;

  //TODO: convert this to CTFE generator
  void doTypedAction(string fnname) () {
         if (lastType[vidx] == VType.Float) mixin(fnname~"(vidx, lastF32[vidx]);");
    else if (lastType[vidx] == VType.Double) mixin(fnname~"(vidx, lastF64[vidx]);");
    else if (lastType[vidx] == VType.Byte) mixin(fnname~"(vidx, cast(ubyte)lastU64[vidx]);");
    else if (lastType[vidx] == VType.Word) mixin(fnname~"(vidx, cast(ushort)lastU64[vidx]);");
    else if (lastType[vidx] == VType.Int) mixin(fnname~"(vidx, cast(uint)lastU64[vidx]);");
    else if (lastType[vidx] == VType.Long) mixin(fnname~"(vidx, cast(ulong)lastU64[vidx]);");
    else assert(0, "invalid type!");
  }

  //TODO: convert this to CTFE generator
  // this throws
  void doTypedSet (uint addr, const(char)[] val) {
    import std.conv : to;
    val = val.xstrip;
    if (val.length == 0) throw new Exception("invalid value");
    if (lastType[vidx] == VType.Float) {
      float v = to!float(val);
      auto atp = AttachedProc(preypid);
      atp.setVal(addr, v);
    } else if (lastType[vidx] == VType.Double) {
      double v = to!double(val);
      auto atp = AttachedProc(preypid);
      atp.setVal(addr, v);
    } else {
      auto atp = AttachedProc(preypid);
           if (lastType[vidx] == VType.Byte) atp.setVal!ubyte(addr, val.xto!ubyte);
      else if (lastType[vidx] == VType.Word) atp.setVal!ushort(addr, val.xto!ushort);
      else if (lastType[vidx] == VType.Int) atp.setVal!uint(addr, val.xto!uint);
      else if (lastType[vidx] == VType.Long) atp.setVal!ulong(addr, val.xto!ulong);
      else assert(0, "wtf?!");
    }
  }

  for (;;) {
    auto res = readLine(vidx);
    if (res != EditLine.Result.Normal) break;
    if (lineedit.line.wordCount == 0) continue;
    auto cmd = lineedit.line.word(0); // command
    if (cmd.length == 0) continue;
    if (cmd == "!!") { injectCode(); continue; }
    if (cmd[0] == '@') {
      if (cmd.length == 1) { lineedit.beep; continue; }
      lineedit.pushCurrentToHistory();
      if (!codeInjected) { writeln("prey is not hunted"); continue; }
      if (cmd == "@@") {
        string[] list = injSock.rpcallany!(string[])("listEndpoints");
        writeln(list.length, " endpoints found");
        foreach (string s; list) writeln("  ", s);
        continue;
      }
      if (cmd == "@asms") {
        if (!injInfo.hasMono) { writeln("prey is colorful"); continue; }
        writeln("=== assemblies ===");
        foreach (const ref ass; monoAsms) {
          writefln("#%s: %s", ass.id, ass.name);
        }
        continue;
      }
      {
        auto wc = lineedit.line.wordCount;
        if (cmd == "@writeclasses") {
          if (wc != 2) { writeln("filename?"); continue; }
          auto arg = lineedit.line.word(1);
          try {
            auto fo = VFile(arg, "w");
            foreach (const ref cls; monoClasses) fo.writefln("0x%08x %s", cls.id, cls.fullname);
          } catch (Exception) {
            writeln("write error with file '", arg, "'");
          }
          continue;
        }
        if (cmd == "@asmname") {
          if (!injInfo.hasMono) { writeln("prey is colorful"); continue; }
          import core.stdc.string : memcpy;
          if (wc != 2) { writeln("assembly id only"); continue; }
          auto arg = lineedit.line.word(1);
          uint asmid = uint.max;
          foreach (immutable idx, const ref ass; monoAsms) if (ass.name == arg) { asmid = cast(uint)idx; break; }
          if (asmid == uint.max) {
            try asmid = arg.xto!uint; catch (Exception) { writeln("invalid assembly id"); continue; }
          }
          if (asmid < monoAsms.length) {
            writefln("assembly #%s has id 0x%08x and name '%s'", asmid, monoAsms[asmid].id, monoAsms[asmid].name);
          } else {
            bool found = false;
            foreach (immutable idx, const ref ass; monoAsms) {
              if (ass.id == asmid) {
                found = true;
                writefln("assembly #%s has id 0x%08x and name '%s'", idx, ass.id, ass.name);
                break;
              }
            }
            if (!found) writeln("assembly not found");
          }
          continue;
        }
        if (cmd == "@classes") {
          if (!injInfo.hasMono) { writeln("prey is colorful"); continue; }
          import core.stdc.string : memcpy;
          if (wc > 2) { writeln("assembly id only"); continue; }
          uint asmid = uint.max;
          if (wc == 2) {
            auto arg = lineedit.line.word(1);
            foreach (immutable idx, const ref ass; monoAsms) if (ass.name == arg) { asmid = cast(uint)idx; break; }
            if (asmid == uint.max) {
              try asmid = arg.xto!uint; catch (Exception) { writeln("invalid assembly id"); continue; }
            }
            if (asmid < monoAsms.length) {
            } else {
              bool found = false;
              foreach (immutable idx, const ref ass; monoAsms) {
                if (ass.id == asmid) {
                  found = true;
                  asmid = cast(uint)idx;
                  break;
                }
              }
              if (!found) { writeln("assembly not found"); continue; }
            }
          }
          foreach (const ref cls; monoClasses) {
            if (asmid == uint.max || cls.asmid == asmid) {
              stderr.writefln("0x%08x %s", cls.id, cls.fullname);
            }
          }
          continue;
        }
        if (cmd == "@fields") {
          if (!injInfo.hasMono) { writeln("prey is colorful"); continue; }
          if (wc != 2) { writeln("class id only"); continue; }
          auto arg = lineedit.line.word(1);
          int cid = findClassByName(arg);
          if (cid < 0) { writeln("class '", arg, "' not found"); continue; }
          //injSock.ncSendCommand(ReqCommand.MonoGetClassFields, monoClasses[cid].id);
          FieldInfo[] list;
          scope(exit) list = null;
          try {
            list = injSock.rpcall!(getClassFields, "mono.")(monoClasses[cid].id);
            writeln("=== ", list, " fields ===");
            foreach (const ref fld; list) {
              writefln("%s %s at 0x%08x (flags=0x%08x)", fld.typename, fld.name, fld.ofs, fld.flags);
            }
          } catch (Exception e) {
            writeln("ERROR: ", e.msg);
          }
          continue;
        }
      }
      writeln("invalid remote command");
      continue;
    }
    if (cmd == "!1") { vidx = 0; continue; }
    if (cmd == "!2") { vidx = 1; continue; }
    if (cmd == "!3") { vidx = 2; continue; }
    if (cmd == "!4") { vidx = 3; continue; }
    if (cmd == "!5") { vidx = 4; continue; }
    if (cmd == "!6") { vidx = 5; continue; }
    if (cmd == "!7") { vidx = 6; continue; }
    if (cmd == "!8") { vidx = 7; continue; }
    if (cmd == "!9") { vidx = 8; continue; }
    if (cmd == "q" || cmd == "quit") break;
    if (cmd == "?") {
      if (!wasSearched[vidx]) {
        writeln("no previous value to filter; type is ", vtName(lastType[vidx]));
      } else {
        write("last search(", vtName(lastType[vidx]), "): ");
             if (lastType[vidx] == VType.Float) writeln(lastF32[vidx]);
        else if (lastType[vidx] == VType.Double) writeln(lastF64[vidx]);
        else writeln(lastU64[vidx]);
      }
      continue;
    }
    if (cmd == ".") {
      if (!wasSearched[vidx]) {
        writeln("no previous value to filter");
      } else {
        doTypedAction!"xfilter"();
      }
      continue;
    }
    lineedit.pushCurrentToHistory();
    if (cmd == "asbyte") { if (lastType[vidx] != VType.Byte) { lastType[vidx] = VType.Byte; wasSearched[vidx] = false; foundAddrs[vidx].clear; } continue; }
    if (cmd == "asshort") { if (lastType[vidx] != VType.Word) { lastType[vidx] = VType.Word; wasSearched[vidx] = false; foundAddrs[vidx].clear; } continue; }
    if (cmd == "asint") { if (lastType[vidx] != VType.Int) { lastType[vidx] = VType.Int; wasSearched[vidx] = false; foundAddrs[vidx].clear; } continue; }
    if (cmd == "aslong") { if (lastType[vidx] != VType.Long) { lastType[vidx] = VType.Long; wasSearched[vidx] = false; foundAddrs[vidx].clear; } continue; }
    if (cmd == "asfloat") { if (lastType[vidx] != VType.Float) { lastType[vidx] = VType.Float; wasSearched[vidx] = false; foundAddrs[vidx].clear; } continue; }
    if (cmd == "asdouble") { if (lastType[vidx] != VType.Double) { lastType[vidx] = VType.Double; wasSearched[vidx] = false; foundAddrs[vidx].clear; } continue; }
    if (cmd == "new") { taskNames[vidx] = null; foundAddrs[vidx].clear; wasSearched[vidx] = false; continue; }
    if (cmd == "killall") {
      taskNames[] = null;
      foreach (ref fv; foundAddrs) fv.clear();
      wasSearched[] = false;
      lastType[] = VType.Int;
      vidx = 0;
      restart(pid);
      continue;
    }
    if (cmd == "list" || cmd == "l" || cmd == "ls") { listMatches(vidx); continue; }
    try {
      import std.conv : to;
           if (lastType[vidx] == VType.Float) lastF32[vidx] = to!float(cmd);
      else if (lastType[vidx] == VType.Double) lastF64[vidx] = to!double(cmd);
      else if (lastType[vidx] == VType.Byte) lastU64[vidx] = xto!ubyte(cmd);
      else if (lastType[vidx] == VType.Word) lastU64[vidx] = xto!ushort(cmd);
      else if (lastType[vidx] == VType.Int) lastU64[vidx] = xto!uint(cmd);
      else if (lastType[vidx] == VType.Long) lastU64[vidx] = xto!ulong(cmd);
      if (wasSearched[vidx]) doTypedAction!"xfilter"(); else doTypedAction!"doNewScanBytes"();
      wasSearched[vidx] = true;
      continue;
    } catch (Exception e) {}
    try {
      auto wc = lineedit.line.wordCount;
      if (wc > 1) {
        auto arg = lineedit.line.word(1);
        if (cmd == "name") {
          if (wc != 2) throw new Exception("name?");
          taskNames[vidx] = arg.idup;
          continue;
        }
        if (cmd == "set") {
          import std.conv : to;
          const(char)[] saddr, sval;
          if (wc == 2 && foundAddrs[vidx].count == 1) {
            saddr = "0";
            sval = arg;
          } else {
            if (wc != 3) throw new Exception("set args fucked");
            saddr = arg;
            sval = lineedit.line.word(2);
          }
          if (saddr.length > 2 && saddr[0..2] == "0x") {
            auto addr = to!uint(saddr[2..$], 16);
            doTypedSet(addr, sval);
          } else if (saddr == "*") {
            foreach (auto va; ASRange(foundAddrs[vidx])) {
              //FIXME: different types
              doTypedSet(va.addr, sval);
            }
          } else {
            auto idx = to!uint(saddr);
            if (idx >= foundAddrs[vidx].count) throw new Exception("invalid value index");
            auto va = foundAddrs[vidx].at(idx);
            doTypedSet(va.addr, sval);
          }
          continue;
        }
        if (cmd == "only") {
          import std.conv : to;
          if (wc != 2) throw new Exception("set args fucked");
          auto saddr = arg;
          if (saddr.length > 2 && saddr[0..2] == "0x") {
            auto addr = to!uint(saddr[2..$], 16);
            foundAddrs[vidx].clear;
            foundAddrs[vidx].addAddress(addr, VType.Int);
          } else {
            auto idx = to!uint(saddr);
            if (idx >= foundAddrs[vidx].count) throw new Exception("invalid value index");
            auto va = foundAddrs[vidx].at(idx);
            foundAddrs[vidx].clear;
            foundAddrs[vidx].addAddress(va.addr, va.vt);
          }
          continue;
        }
      }
    } catch (Exception e) {
      writeln("ERROR: ", e.msg);
      continue;
    }
    writeln("invalid command");
  }
}
